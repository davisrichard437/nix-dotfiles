{
  global = {
    ### Display ###;
    follow = "mouse";
    # width = 300;
    # height = 500;
    # offset = "30x30";
    geometry = "300x5-30+30";
    progress_bar = "true";
    progress_bar_height = 10;
    progress_bar_frame_width = 1;
    progress_bar_min_width = 150;
    progress_bar_max_width = 300;
    indicate_hidden = "yes";
    shrink = "no";
    transparency = 0;
    notification_height = 0;
    separator_height = 2;
    padding = 8;
    horizontal_padding = 8;
    text_icon_padding = 0;
    frame_width = 3;
    frame_color = "#bd93f9";
    separator_color = "frame";
    sort = "yes";
    idle_threshold = 120;
    ### Text ###;
    font = "Hack 12";
    line_height = 0;
    markup = "full";
    format = "<b>%s</b>\\n%b";
    alignment = "left";
    vertical_alignment = "center";
    show_age_threshold = -1;
    ignore_newline = "no";
    stack_duplicates = true;
    hide_duplicate_count = false;
    show_indicators = "yes";
    ### Icons ###
    icon_position = "left";
    min_icon_size = 0;
    max_icon_size = 32;
    ### History ###
    sticky_history = "yes";
    history_length = 20;
    ### Misc/Advanced ###;
    browser = "firefox -new-tab";
    always_run_script = true;
    title = "Dunst";
    class = "Dunst";
    startup_notification = false;
    verbosity = "mesg";
    corner_radius = 6;
    ignore_dbusclose = false;
    ### mouse ###
    mouse_left_click = "close_current";
    mouse_middle_click = "do_action, close_current";
    mouse_right_click = "close_all";
  };

  urgency_low = {
    background = "#282a36";
    foreground = "#8be9fd";
    timeout = 10;
  };

  urgency_normal = {
    background = "#282a36";
    foreground = "#f8f8f2";
    timeout = 10;
  };

  urgency_critical = {
    background = "#282a36";
    foreground = "#ff5555";
    frame_color = "#ff5555";
    timeout = 0;
  };
}

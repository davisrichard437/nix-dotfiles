{ laptop, nixOS }:

{
  "l." = "eza -a --group-directories-first | egrep '^\\.'";
  cat = "bat";
  cleanup =
    if nixOS then
      "nix-collect-garbage -d"
    else
      "nix-collect-garbage -d; sudo pacman -Rns $(pacman -Qtdq)";
  df = "df -h -x squashfs -x tmpfs -x devtmpfs";
  du = "du -h";
  egrep = "egrep --color=auto";
  fgrep = "fgrep --color=auto";
  free = "free -mt";
  git-update = "git commit -am 'Minor updates.' && git push";
  grep = "grep --color=auto";
  hs =
    if laptop then
      "home-manager --flake ~/.dotfiles/#non-nixos-laptop switch -b backup"
    else
      "home-manager --flake ~/.dotfiles/#non-nixos-desktop switch -b backup";
  ix = "curl -F 'f:1=<-' ix.io";
  l = "eza --group-directories-first";
  la = "eza -lah --group-directories-first";
  ll = "eza -lah --group-directories-first";
  ls = "eza -l --group-directories-first";
  merge = "xrdb -merge $HOME/.Xresources";
  mpvc = "mpv \"$(wl-paste)\"";
  ns = "sudo nixos-rebuild switch --flake ~/.dotfiles/#";
  pacman = "pacman --color auto";
  pandoc =
    "pandoc -V margin-top=1in -V margin-left=1in -V margin-right=1in -V margin-bottom=1in -V papersize=letter";
  pkgcount = "pacman -Q | wc -l";
  psa = "ps auxf";
  psgrep = "ps aux | grep -v grep | grep -i -e VSZ -e";
  rf = "readlink -f";
  rg = "rg --sort path";
  sbcl = "rlwrap sbcl";
  sn = "shutdown now";
  speedtest =
    "curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -";
  sr = "reboot";
  su = "sudo -i";
  tree = "eza -T";
  update-fc = "sudo fc-cache -fv";
  update-grub = "sudo grub-mkconfig -o /boot/grub/grub.cfg";
  userlist = "cut -d: -f1 /etc/passwd";
  vb = "$EDITOR ~/.bashrc";
  vconfgrub = "sudo $EDITOR /boot/grub/grub.cfg";
  vfstab = "sudo $EDITOR /etc/fstab";
  vgrub = "sudo $EDITOR /etc/default/grub";
  vlightdm = "sudo $EDITOR /etc/lightdm/lightdm.conf";
  vmirrorlist = "sudo $EDITOR /etc/pacman.d/mirrorlist";
  vmkinitcpio = "sudo $EDITOR /etc/mkinitcpio.conf";
  vnsswitch = "sudo $EDITOR /etc/nsswitch.conf";
  vpacman = "sudo $EDITOR /etc/pacman.conf";
  vsamba = "sudo $EDITOR /etc/samba/smb.conf";
  vsddm = "sudo $EDITOR /etc/sddm.conf";
  vz = "$EDITOR ~/.zshrc";
  wttr = "curl wttr.in\\?m";
  ymd = "date +%F";
  yta-aac = "youtube-dl --extract-audio --audio-format aac ";
  yta-best = "youtube-dl --extract-audio --audio-format best ";
  yta-flac = "youtube-dl --extract-audio --audio-format flac ";
  yta-m4a = "youtube-dl --extract-audio --audio-format m4a ";
  yta-mp3 = "youtube-dl --extract-audio --audio-format mp3 ";
  yta-opus = "youtube-dl --extract-audio --audio-format opus ";
  yta-vorbis = "youtube-dl --extract-audio --audio-format vorbis ";
  yta-wav = "youtube-dl --extract-audio --audio-format wav ";
  ytv-best = "youtube-dl -f bestvideo+bestaudio ";
}

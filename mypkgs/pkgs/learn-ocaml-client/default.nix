{ pkgs, coreutils, gnumake }:

with pkgs;

stdenv.mkDerivation rec {
  name = "learn-ocaml-client";
  version = "0.14.1";

  src = fetchurl {
    url =
      "https://github.com/ocaml-sf/learn-ocaml/releases/download/v${version}/learn-ocaml-client-linux-x86_64";
    sha256 = "sha256-dfvsWBtrQ1c56W8/AfzHw9sYqBzcoDF3rDeGEKeoLe8=";
  };

  propogatedBuildInputs = [ gnumake ];

  builder = writeShellScript "builder.sh" ''
    set -e
    export PATH=${coreutils}/bin
    install -Dm755 $src $out/bin/learn-ocaml-client
  '';
}

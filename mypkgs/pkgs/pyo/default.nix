{ fetchFromGitHub
, python3Packages
, liblo
, libsndfile
, portaudio
, portmidi
, wxPython_4_2
, buildPythonPackage
}:

buildPythonPackage rec {
  pname = "pyo";
  version = "1.0.5";
  system = builtins.currentSystem;

  src = fetchFromGitHub {
    owner = "belangeo";
    repo = "${pname}";
    rev = "6b23e1af51d81509f58882b297b58e6b40bbf093";
    sha256 = "b2PiGTvG2PvNZYkxtjXlB52938l2jqLaDh4Y9wupwRM=";
  };

  # needs to access files?
  doCheck = false;

  propagatedBuildInputs = [
    wxPython_4_2
    liblo
    libsndfile
    portaudio
    portmidi
  ];
}

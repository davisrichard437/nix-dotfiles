{ python3
, numpy
, wxPython_4_2
, pyo
, six
, mkDerivation
, fetchFromGitHub
, writeShellScript
, coreutils
}:

mkDerivation rec {
  name = "cecilia5";
  version = "5.4.1";
  system = builtins.currentSystem;

  src = fetchFromGitHub {
    owner = "belangeo";
    repo = "${name}";
    rev = "8e97a592a5569db099f32bb19db246077d85d58d";
    sha256 = "R0Xazl3Gbz+EBchsrn+ZK7l/7PH8WOcCpJoKGFJrCWQ=";
  };

  nativeBuildInputs = [ coreutils ];
  buildInputs = [
    python3
    wxPython_4_2
    pyo
    numpy
    six
  ];

  builder = writeShellScript "builder.sh" ''
    export PATH=${coreutils}/bin

    mkdir -p $out/bin/
    mkdir -p $out/share/applications/
    mkdir -p $out/share/pixmaps/
    mkdir -p $out/lib/${name}/

    install -Dm444 "$src/Cecilia5.py" $out/lib/${name}/Cecilia5.py
    cp -r --no-preserve=ownership --preserve=mode $src/Resources/ $out/lib/${name}/Resources/

    cat << EOF > $out/bin/${name}
    #!/bin/sh
    export PYTHONPATH=${wxPython_4_2}/lib/python3.10/site-packages:${numpy}/lib/python3.10/site-packages:${pyo}/lib/python3.10/site-packages:${six}/lib/python3.10/site-packages
    echo \$PYTHONPATH
    cd $out/lib/${name}
    exec ${python3}/bin/python Cecilia5.py "\$@"
    EOF
    chmod 555 $out/bin/${name}

    cat << EOF > $out/share/applications/${name}.desktop
    [Desktop Entry]
    Type=Application
    Name=Cecilia
    Comment=Audio signal processing environment aimed at sound designers
    Icon=cecilia
    Exec=cecilia5
    Terminal=false
    Categories=AudioVideo;Audio;
    EOF

    ln -sf $out/lib/Resources/Cecilia5.ico $out/share/pixmaps/Cecilia5.ico
  '';
}

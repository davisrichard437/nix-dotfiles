{ pkgs, lib, mkDerivation, fetchFromGitLab, coreutils, writeShellScript }:

mkDerivation rec {
  name = "xsudo";
  version = "1.0.1";
  system = builtins.currentSystem;

  meta = with lib; {
    description = "A simple shell-script gksudo replacement";
    longDescription = ''
      xsudo is a simple shell script to run graphical GNU/Linux applications
      with root permissions via pkexec. It is taken from the following page on
      the LXDE wiki:
      http://web.archive.org/web/20220428194115/https://wiki.lxde.org/en/PCManFM#pkexec_method
    '';
    homepage = "https://gitlab.com/davisrichard437/xsudo/";
    license = licenses.mit;
    platform = platforms.linux;
  };

  src = fetchFromGitLab {
    owner = "davisrichard437";
    repo = "${name}";
    rev = "v${version}";
    sha256 = "wVRM7aIbsvop1X8EGdFccZ9sl21mpZcaHbWnO6Jba+w=";
  };

  builder = pkgs.writeShellScript "builder.sh" ''
    set -e
    export PATH=${coreutils}/bin
    cd $src
    install -Dm755 xsudo $out/bin/xsudo
  '';
}

{ stdenv
, gnat
, gprbuild
, fetchFromGitHub
, writeShellScriptBin
, symlinkJoin
}:

let
  lib = stdenv.mkDerivation {
    pname = "words";
    version = "1.2.3";

    src = fetchFromGitHub {
      owner = "mk270";
      repo = "whitakers-words";
      rev = "9b11477";
      sha256 = "sha256-f/8dQff2min0FivBKTk/kcwwoW5IfajAjsdkALT52xU=";
    };

    nativeBuildInputs = [
      gprbuild
      gnat
    ];

    dontConfigure = true;

    buildPhase = ''
      runHook preBuild

      make

      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      mkdir -p $out/{bin,lib/words}

      cp -r bin $out/lib/words/bin
      cp -r lib $out/lib/words/lib
      cp *.GEN $out/lib/words/
      cp *.LAT $out/lib/words/
      cp *.SEC $out/lib/words/

      runHook postInstall
    '';
  };

  script = writeShellScriptBin "words"
    ''
      cd ${lib}/lib/words
      ./bin/words
    '';

in

symlinkJoin {
  name = "words";
  paths = [ lib script ];
}

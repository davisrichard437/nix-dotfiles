{ lib, fetchurl, fetchFromGitHub, buildDunePackage, ocamlPackages }:

buildDunePackage rec {
  pname = "markup-lwt";
  version = "1.0.3";
  duneVersion = "2";
  minimumOCamlVersion = "4.03.0";

  src = fetchurl {
    url =
      "https://github.com/aantron/markup.ml/archive/refs/tags/${version}.tar.gz";
    sha256 = "sha256-lSb9BqCvw3165uJSh4cULVKxJCOP+w5+joO904OAbrU=";
  };

  buildInputs = with ocamlPackages; [ markup lwt ];
}

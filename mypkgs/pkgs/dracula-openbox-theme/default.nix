{ pkgs
, lib
, mkDerivation
, fetchFromGitHub
, coreutils
, writeShellScript
}:

mkDerivation rec {
  name = "dracula-openbox-theme";
  version = "1.0";
  system = builtins.currentSystem;

  src = fetchFromGitHub {
    owner = "dracula";
    repo = "openbox";
    rev = "489fdb58400db74e48369d0c67e808bedf65865f";
    sha256 = "oxPGcR3+lzJaDvtg/Deuvw5+DKKZo9LD2x0i6cTZu8M=";
  };

  builder = writeShellScript "builder.sh" ''
    set -e
    export PATH=${coreutils}/bin
    cd $src
    mkdir -p $out/share/themes
    cp -r Dracula $out/share/themes/Dracula
    cp -r Dracula-withoutBorder $out/share/themes/Dracula-withoutBorder
  '';
}

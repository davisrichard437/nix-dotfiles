{ lib, fetchFromGitHub, buildDunePackage, ocamlPackages, ocplib-json-typed }:

buildDunePackage rec {
  _name = "ocplib-json-typed";
  pname = "${_name}-browser";
  version = "0.7";
  duneVersion = "3";
  minimumOCamlVersion = "4.03.0";

  src = fetchFromGitHub {
    owner = "OCamlPro";
    repo = "${_name}";
    rev = "v${version}";
    sha256 = "sha256-VWNfj1BjDMJGCM1GCCkWAGreGt2QDOtath43kVreao8=";
  };

  buildInputs = [ ocplib-json-typed ]
    ++ (with ocamlPackages; [ js_of_ocaml uri ]);

  patches = [ ./0001-Adding-dune-project.patch ];
}

{ pkgs, coreutils, mkDerivation, libX11, fetchFromGitHub }:

mkDerivation rec {
  name = "xclickroot";
  version = "1.3";
  system = builtins.currentSystem;

  src = fetchFromGitHub {
    owner = "phillbush";
    repo = "${name}";
    rev = "v${version}";
    sha256 = "U8xETpScmrEItcpUCoWyTcg1nTRWQC+lyAJsAnd32nI=";
  };

  buildInputs = [ libX11 ];

  installPhase = ''
    runHook preInstall
    make PREFIX="" DESTDIR=$out install
    runHook postInstall
  '';
}

{ lib
, fetchFromGitHub
, buildDunePackage
, ocamlPackages
, markup-lwt
, ocplib-json-typed-browser
}:

buildDunePackage rec {
  pname = "learn-ocaml";
  version = "0.14.1";
  minimumOCamlVersion = "4.03.0";
  duneVersion = "3";

  buildInputs = with ocamlPackages; [
    cmdliner
    cohttp-lwt-unix
    decompress
    digestif
    ezjsonm
    js_of_ocaml
    js_of_ocaml-ppx
    js_of_ocaml-tyxml
    lwt_react
    markup
    markup-lwt
    ocp-ocamlres
    ocplib-json-typed-browser
    omd
    ppx_tools
    vg
  ];

  src = fetchFromGitHub {
    owner = "ocaml-sf";
    repo = "${pname}";
    rev = "v${version}";
    sha256 = "QnFjB477cIsmgLLMAba8z6Q0lEcUYHeu3H35e5LfXJk=";
  };

  patches = [ ./0001-Bumping-dune-version.patch ];

  meta.broken = true;
}

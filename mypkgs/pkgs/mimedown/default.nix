{ mkDerivation
, coreutils
, python3
}:

mkDerivation rec {
  name = "mimedown";
  version = "0.1.0";
  system = builtins.currentSystem;

  src = ./.;

  propagatedBuildInputs = [
    (python3.withPackages
      (ps: with ps; [ markdown ]))
  ];

  dontStrip = true; # needed to build with flakes

  installPhase = ''
    cd $src
    install -Dm755 mimedown $out/bin/mimedown
  '';
}

{ lib, fetchFromGitHub, buildDunePackage, ocamlPackages, }:

buildDunePackage rec {
  pname = "ocplib-json-typed";
  version = "0.7";
  duneVersion = "2";
  minimumOCamlVersion = "4.03.0";

  src = fetchFromGitHub {
    owner = "OCamlPro";
    repo = "${pname}";
    rev = "v${version}";
    sha256 = "sha256-VWNfj1BjDMJGCM1GCCkWAGreGt2QDOtath43kVreao8=";
  };

  buildInputs = [ ocamlPackages.uri ];
}

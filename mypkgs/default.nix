{ nixpkgs ? import <nixpkgs> { }, stdenv ? nixpkgs.stdenv, ... }:

let

  allPkgs = nixpkgs // pkgs;
  lib = nixpkgs.lib;
  mylib = import ../lib { pkgs = allPkgs; };
  inherit (mylib) wrap adwLaunch callPackage;
  myNixGL = nixpkgs.nixgl.nixGLIntel;

  pkgs = with nixpkgs; rec {
    # bring buildInputs into scope
    inherit coreutils fetchurl fetchFromGitHub fetchFromGitLab ocamlPackages
      pkgs writeShellScript writeShellScriptBin lib stdenv gnumake symlinkJoin;
    inherit (stdenv) mkDerivation;
    inherit (ocamlPackages) buildDunePackage;
    inherit (xorg) libX11;
    python3 = python310;
    inherit (python310Packages)
      pillow pygobject3 setuptools xlib buildPythonApplication
      buildPythonPackage mock nose wxPython_4_2 numpy six markdown;
    inherit liblo libsndfile portaudio portmidi;
    inherit scrot xclip xdg-utils gettext gtk3 pango gdk-pixbuf
      gobject-introspection;
    inherit fiji;

    # working:
    xsudo = callPackage ./pkgs/xsudo { };
    xclickroot = callPackage ./pkgs/xclickroot { };
    markup-lwt = callPackage ./pkgs/markup-lwt { };
    ocplib-json-typed = callPackage ./pkgs/ocplib-json-typed { };
    ocplib-json-typed-browser =
      callPackage ./pkgs/ocplib-json-typed-browser { };
    learn-ocaml-client = callPackage ./pkgs/learn-ocaml-client { };
    pyo = callPackage ./pkgs/pyo { };
    cecilia = callPackage ./pkgs/cecilia { };
    mimedown = callPackage ./pkgs/mimedown { };
    dracula-openbox-theme = callPackage ./pkgs/dracula-openbox-theme { };
    words = callPackage ./pkgs/words { };
    qutebrowser-nixgl = wrap rec {
      pkgs = allPkgs;
      package = pkgs.qutebrowser;
      nixgl = myNixGL;
      nixglName = "nixGLIntel";
      binary = "qutebrowser";
    };
    alacritty-nixgl = wrap
      rec {
        pkgs = allPkgs;
        package = pkgs.alacritty;
        nixgl = myNixGL;
        nixglName = "nixGLIntel";
        binary = "alacritty";
      } // { version = allPkgs.alacritty.version; };
    fiji-adw = adwLaunch {
      pkgs = allPkgs;
      package = pkgs.fiji;
      binary = "fiji";
    };

    # not working:
    learn-ocaml = callPackage ./pkgs/learn-ocaml { };
  };

in
with pkgs;
{
  inherit xsudo xclickroot pyo cecilia qutebrowser-nixgl alacritty-nixgl
    learn-ocaml-client mimedown dracula-openbox-theme fiji-adw words;
}

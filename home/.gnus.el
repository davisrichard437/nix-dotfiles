;; I don't know how many of these I actually need, just grabbing all
(require 'auth-source)
(require 'auth-source-pass)
(require 'password-store)

(setq auth-sources '(password-store "~/.authinfo.gpg"))

(setq user-mail-address "davisrichard437@gmail.com"
      user-full-name "Richard Davis")

(setq gnus-select-method '(nnnil ""))
(setq gnus-secondary-select-methods
      '((nnimap "gmail"
                (nnimap-address "imap.gmail.com")
                (nnimap-user "davisrichard437@gmail.com")
                (nnimap-server-port "imaps")
                (nnimap-stream ssl))
        (nnimap "outlook"
                (nnimap-address "127.0.0.1")
                (nnimap-user "richard.davis@mail.mcgill.ca")
                (nnimap-server-port "1143")
                (nnimap-stream plain)
                (nnimap-search-engine imap))))

(setq gnus-posting-styles
      '((".*"
         (address "davisrichard437@gmail.com"))
        ("outlook"
         (address "richard.davis@mail.mcgill.ca")
         ("X-Message-SMTP-Method"
          "smtp 127.0.0.1 1025 richard.davis@mail.mcgill.ca"))))

(setq smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")

(setq message-kill-buffer-on-exit t
      message-send-mail-function 'smtpmail-send-it)

(setq gnus-gcc-mark-as-read t)
(setq shr-width 80
      shr-use-fonts nil
      shr-use-colors nil)

(defun mimedown ()
  "Translate a markdown email into html and alternate plaintext regions."
  (interactive)
  (save-excursion
    (message-goto-body)
    (shell-command-on-region (point) (point-max) "mimedown" nil t)))

(defun my/compose-mail ()
  "Start composing a mail message to send according to the selected
style from `gnus-posting-styles'."
  (interactive)
  (let* ((posting-style-pattern (completing-read "Outbox: " gnus-posting-styles))
         (posting-style (alist-get posting-style-pattern
                                   gnus-posting-styles
                                   nil nil #'string=))
         (headers (mapcar
                   (lambda (l)
                     (if (eq (car l) 'address)
                         (list "From"
                               (format "%s <%s>" user-full-name (cadr l)))
                       l))
                   posting-style)))
    (compose-mail nil nil headers)))

(add-hook 'message-mode-hook #'flyspell-mode)

#!/usr/bin/env bash
# obmenu.sh - format openbox pipe menu

xdgmenumaker --format compizboxmenu 2>/dev/null\
    | sed -e '1c <openbox_pipe_menu>'\
          -e '$c </openbox_pipe_menu>'\
          -e 's/<menu name="\(\w*\)\">/<menu id="\1" label="\1">/'\
          -e 's/<item type="launcher"><name>\(.*\)<\/name><command>\(.*\)<\/command>.*/<item label="\1"><action name="Execute"><execute>\2<\/execute><\/action><\/item>/'

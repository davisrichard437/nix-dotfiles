#!/usr/bin/env bash
# ~/.config/qtile/scripts/wttr-location.sh
# Script to get current location from wttr

curl -s wttr.in/\?0mT\
    | sed -n '1s/Weather report: \([^,]*\),.*$/\1/p'

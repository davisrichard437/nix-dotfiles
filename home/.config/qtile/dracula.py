# dracula.py
# colors resource for dracula theme

colors = {
    "foreground": "f8f8f2",
    "background": "282a36",
    "focus": "bd93f9",
    "focus2": "bd93f9",
    "urgent": "ff5555",
}

# ~/.config/qtile/config.py
# the main config file for the Qtile window manager.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
# Following 2 for autostart applications
import os
import subprocess
# For color schemes.
from dracula import colors

mod = "mod4"                    # mod = Super key
terminal = "alacritty"

keys = [ # STARTKEYS

    # KBGROUP Window Manipulation:
    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.next(), desc="Move focus to next window"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "l", lazy.screen.next_group(), desc="Move to right group"),
    Key([mod], "h", lazy.screen.prev_group(), desc="Move to left group"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),
    Key([mod], "n", lazy.window.toggle_minimize(), desc="Toggle minimize"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # KBGROUP Layout Manipulation:
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), lazy.layout.grow(), lazy.layout.increase_ratio(), lazy.layout.delete(), desc="Grow window left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), lazy.layout.grow(), lazy.layout.increase_ratio(), lazy.layout.delete(), desc="Grow window right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), lazy.layout.grow(), lazy.layout.increase_ratio(), lazy.layout.delete(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), lazy.layout.grow(), lazy.layout.increase_ratio(), lazy.layout.delete(), desc="Grow window up"),
    Key([mod, "shift"], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating"),
    Key([mod, "control"], "f", lazy.layout.flip(), desc="Flip monad layouts"),

    # KBGROUP Qtile:
    Key([mod], "q", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # KBGROUP Spawn:
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod, "shift"], "Return", lazy.spawn("pcmanfm"), desc="Launch file manager"),
    Key([mod], "a", lazy.spawn("arandr"), desc="Run arandr"),
    Key([mod], "r", lazy.spawn("rofi -modi 'drun' -show drun"), desc="Run rofi"),
    Key([mod, "shift"], "w", lazy.spawn("rofi -show window"), desc="rofi window switcher"),
    Key([mod, "shift"], "r", lazy.spawn("gmrun"), desc="gmrun"),
    Key([mod], "f", lazy.spawn("pcmanfm"), desc="Launch file manager"),
    Key([mod], "b", lazy.spawn("firefox"), desc="Run Firefox"),
    Key([mod, "shift"], "b", lazy.spawn("firefox --private-window"), desc="Run Firefox in private mode"),
    Key([mod], "p", lazy.spawn("pavucontrol"), desc="Run mixer"),
    Key([mod], "u", lazy.spawn("alacritty -t 'Update' -e .local/bin/update"), desc="Update"),
    Key([mod], "v", lazy.spawn("vmpk"), desc="Run vmpk"),
    Key([mod], "x", lazy.spawn("xkill"), desc="Run xkill"),
    Key([mod, "shift"], "v", lazy.spawn("vlc"), desc="Run vlc"),
    Key([mod], "m", lazy.spawn("mscore"), desc="Run musescore"),
    Key([mod], "d", lazy.spawn("reaper"), desc="Run reaper"), # reaper no longer crashes qtile?
    Key([mod, "shift"], "d", lazy.spawn("audacity"), desc="Run audacity"),
    Key([mod], "c", lazy.spawn(".config/picom/picom-toggle.sh"), desc="Restart picom"),
    Key([mod, "shift"], "c", lazy.spawn(".local/bin/color-scheme"), desc="Switch color scheme"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn(".local/bin/volume -i 5"), desc="Raise volume"),
    Key([], "XF86AudioLowerVolume", lazy.spawn(".local/bin/volume -d 5"), desc="Lower volume"),
    Key([], "XF86AudioMute", lazy.spawn(".local/bin/volume -t"), desc="Mute volume"),
    Key([], "Print", lazy.spawn("gscreenshot"), desc="Open screenshot utility"),
    Key([], "XF86MonBrightnessUp", lazy.spawn(".local/bin/brightness 5%+"), desc="Raise brightness"),
    Key([], "XF86MonBrightnessDown", lazy.spawn(".local/bin/brightness 5%-"), desc="Lower brightness"),
    Key([mod], "e", lazy.spawn("emacsclient -c -a emacs"), desc="Open emacs"),
    Key([mod, "shift"], "e", lazy.spawn("emacs -Q"), desc="Open base emacs"),
    Key([mod, "control"], "e", lazy.spawn(".local/bin/rofi-edit"), desc="Rofi edit prompt"),
    Key([mod], "s", lazy.spawn(".local/bin/rofi-power-menu"), desc="Rofi power prompt"),
    Key([mod, "shift"], "s", lazy.spawn(".config/qtile/scripts/keys.sh"), desc="List keybindings"),
] # ENDKEYS

group_names = ["WWW", "MUS", "DEV", "MSG", "DOC", "MED", "SYS", "MSC"]

groups = [Group(name) for name in group_names]

for i, (name) in enumerate(group_names, 1):
    # Switch to another group
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    # Send current window to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))

layout_theme = {
    "border_width": 2,
    "margin": 6,
    "border_focus": colors["focus"],
    "border_normal": colors["background"],
}

layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Floating(**layout_theme),
    # layout.Columns(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Hack',
    fontsize=16,
    padding=3,
    foreground=colors["foreground"],
    background=colors["background"],
)
extension_defaults = widget_defaults.copy()

# Get Wttr data for current location
home = os.path.expanduser("~")
wttr_location_script = ".config/qtile/scripts/wttr-location.sh"
wttr_location = os.popen(f"{home}/{wttr_location_script}").read().strip()

screens = [
    Screen(
        top=bar.Bar(
            [
                # Left widgets
                widget.CurrentLayoutIcon(),
                widget.Spacer(length=6),
                widget.GroupBox(
                    disable_drag=True,
                    invert_mouse_wheel=False,
                    highlight_method="line",
                    this_current_screen_border=[colors["focus2"], colors["focus2"]],
                    highlight_color=[colors["focus"], colors["focus"]],
                    inactive=colors["foreground"],
                    active=colors["foreground"],
                    urgent_border=colors["urgent"],
                ),
                widget.Spacer(length=6),
                # widget.WindowName(),
                widget.TaskList(
                    highlight_method="block",
                    rounded=False,
                    spacing=6,
                    border=colors["focus2"],
                    this_current_screen_border=[colors["focus2"], colors["focus2"]],
                    highlight_color=[colors["focus"], colors["focus"]],
                    inactive=colors["foreground"],
                    active=colors["foreground"],
                    urgent_border=colors["urgent"],
                    margin=0,
                ),

                # Right widgets
                widget.Systray(icon_size=22, padding=5),
                widget.Wttr(
                    format = " | %l: %t",
                    location = {wttr_location: wttr_location},
                ),
                widget.Clock(format=' | %a, %d %b %Y, %H:%M '),
            ],
            opacity=1.0,
            size=24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button2", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button3", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = "floating_only"
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        Match(wm_type='utility'),
        Match(wm_type='notification'),
        Match(wm_type='toolbar'),
        Match(wm_type='splash'),
        Match(wm_type='dialog'),
        Match(wm_class='file_progress'),
        Match(wm_class='confirm'),
        Match(wm_class='dialog'),
        Match(wm_class='download'),
        Match(wm_class='error'),
        Match(wm_class='feh'),
        Match(wm_class='gmrun'),
        Match(wm_class='notification'),
        Match(wm_class='splash'),
        Match(wm_class='toolbar'),
        Match(wm_class='confirmreset'),      # gitk
        Match(wm_class='makebranch'),        # gitk
        Match(wm_class='maketag'),           # gitk
        Match(wm_class='ssh-askpass'),       # ssh-askpass
        Match(wm_class='Gnome-calculator'),  # calculator
        Match(wm_class='Galculator'),        # calculator
        Match(wm_class='Mate-calc'),         # calculator
        Match(wm_class='Gnome-screenshot'),  # screenshot
        Match(wm_class='Gscreenshot'),       # screenshot
        Match(wm_class='.gscreenshot-wrapped'),  # screenshot
        Match(wm_class='VMPK'),              # VMPK
        Match(wm_class='pinentry'),          # GPG key password entry
        Match(title='Discord Updater'),      # Discord startup
        Match(title='branchdialog'),         # gitk
        Match(title='win0'),                 # fiji
        Match(title='(Fiji Is Just) ImageJ'), # fiji
    ],
    **layout_theme,
)

# My hooks
# Autostart applications in ~/.config/qtile/autostart.sh
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    scripts = [                 # add more as necessary
        '.config/scripts/autostart',
    ]
    try:
        for script in scripts:
            subprocess.Popen([f"{home}/{script}"])
    except Exception as e:
        with open('qtile_log', 'a+') as f:
            f.write(str(e) + '\n')

# Lists of substrings to send to each group
sends = [
    ["firefox", "brave", "qutebrowser", "thunderbird"],                # WWW
    ["musescore", "midi", "audacity", "reaper"],                       # MUS
    [],                                                                # DEV
    ["zoom", "conference", "discord", "signal"],                       # MSG
    ["libreoffice"],                                                   # DOC
    ["vlc", "kdenlive", "win0", "(Fiji Is Just) ImageJ"],              # MED
    ["volume control", "add/remove software", "screen layout editor"], # SYS
    [],                                                                # MSC
]

# Sending clients to appropriate groups
@hook.subscribe.client_new
def func(c):
    for i, ws_substrs in enumerate(sends):
        if any(substring in c.name.lower() for substring in ws_substrs):
            c.togroup(group_names[i])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

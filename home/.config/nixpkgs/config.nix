{
  allowUnfree = true;
  packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball {
      url = "https://github.com/nix-community/NUR/archive/master.tar.gz";
      # sha256 = "0jxf6ynq43arsyglm8zy9l840i7hcgyakmxz0292nix55ykdr2xd";
    }) { inherit pkgs; };
  };
}

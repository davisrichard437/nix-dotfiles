(define-skeleton skeleton-comp202-python
  "Inserts header for COMP 202."
  "File description: "
  "# Author: "
  user-full-name
  "\n# McGill ID: 260920526\n"
  "# Group: 17c\n"
  "# " (file-name-nondirectory (buffer-file-name))
  " - " str
  "\n\n" "import doctest"
  "\n\n" _ "\n\n"
  "if __name__ == \"__main__\":\n"
  "    doctest.testmod()")

(define-skeleton skeleton-python
  "Inserts header for COMP 202."
  "File description: "
  "# Author: "
  user-full-name
  "\n# " (file-name-nondirectory (buffer-file-name))
  " - " str
  "\n\n" _ "\n\n"
  "if __name__ == \"__main__\":\n"
  "    quit()")

(define-skeleton skeleton-shell
  "Inserts shell script header skeleton."
  "File description: "
  "#!/usr/bin/env bash\n"
  "# " (file-name-nondirectory (buffer-file-name))
  " - " str
  "\n\n"
  _)

(define-skeleton skeleton-latex-document
  "Inserts a template for basic LaTeX documents."
  ""
  "\\documentclass[letterpaper,12pt]{article}\n"
  "\\usepackage[T1]{fontenc}\n"
  "\\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}\n\n"
  "\\begin{document}\n"
  _
  "\n\\end{document}")

(define-skeleton skeleton-latex-list
  "Inserts a skeleton for an itemized list."
  ""
  "\\begin{itemize}\n"
  "  \\item " _ "\n"
  "\\end{itemize}")

(define-skeleton skeleton-latex-mla
  "Inserts a skeleton for an MLA-style document."
  ""
  "\\newcommand{\\firstName}{" (skeleton-read "First name: ") "}\n"
  "\\newcommand{\\lastName}{" (skeleton-read "Last name: ") "}\n"
  "\\documentclass[letterpaper,12pt]{article}\n"
  "\\usepackage[T1]{fontenc}\n"
  "\\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}\n"
  "\\usepackage{setspace}\n"
  "\\doublespacing\n"
  "\\usepackage{times}\n"
  "\\usepackage{fancyhdr}\n"
  "\\pagestyle{fancy}\n"
  "\\lhead{}\n"
  "\\chead{}\n"
  "\\rhead{\\lastName \\ \\thepage}\n"
  "\\lfoot{}\n"
  "\\cfoot{}\n"
  "\\rfoot{}\n"
  "\\renewcommand{\\headrulewidth}{0pt}\n"
  "\\renewcommand{\\footrulewidth}{0pt}\n"
  "\\usepackage{titlesec}\n"
  "\\usepackage{indentfirst}\n"
  "%To make sure we actually have header 0.5in away from top edge\n"
  "%12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value\n"
  "\\setlength\\headsep{0.333in}\n\n"
  "\\begin{document}\n"
  "\\noindent \\firstName \\ \\lastName\\\\\n"
  "Prof. " (skeleton-read "Prof: ") "\\\\\n"
  (skeleton-read "Class: ") "\\\\\n"
  "\\today\n\n"
  "\\begin{center}\n"
  (skeleton-read "Title: ") "\n"
  "\\end{center}\n"
  "\\setlength{\\parindent}{0.5in}\n"
  "\\titleformat{\\section}{\\normalfont\\normalsize\\bfseries}{}{1em}{}\n\n"
  _ "\n\n"
  "\\end{document}")

(define-skeleton skeleton-latex-italics
  "Insert LaTeX italic text."
  ""
  "\\textit{" _ "}")

(define-skeleton skeleton-latex-bold
  "Insert LaTeX bold text."
  ""
  "\\textbf{" _ "}")

(use-package latex
  :ensure nil
  :mode ("\\.tex\\'" . latex-mode)
  :bind
  ;; (:map LaTeX-mode-map
  ;;       ("C-c i" . latex-italics)
  ;;       ("C-c b" . latex-bold))
  )

(define-skeleton skeleton-rust-test-module
  "Inserts a skeleton for a rust test module."
  "Test name: "
  "#[cfg(test)]\n"
  "mod test {\n"
  "    #[test]\n"
  (when (y-or-n-p "Panic test: ") "    #[should_panic]\n")
  "    fn " str "() {\n"
  "        " _ "\n"
  "        assert_eq!();\n"
  "    }\n"
  "}")

(define-skeleton skeleton-rust-test
  "Inserts a skeleton for a rust test."
  "Test name: "
  "    #[test]\n"
  (when (y-or-n-p "Panic test: ") "    #[should_panic]\n")
  "    fn " str "() {\n"
  "        " _ "\n"
  "        assert_eq!();\n"
  "    }\n")

(define-skeleton skeleton-lilypond-roman-numeral
  "Insert a Roman Numeral in Lilypond."
  "Numeral: "
  "_\"" str "\"")

(define-skeleton skeleton-lytex-section
  "LyTex section."
  ""
  "\\begin{lilypond}\n"
  (when (y-or-n-p "English accidentals? ")
    "  \\language \"english\"\n")
  "  \\score {\n"
  "    \\new Staff {\n"
  "      " _ "\n"
  "    }\n"
  "  }\n"
  "\\end{lilypond}")

(define-skeleton skeleton-org-document
  "Insert skeleton for org mode documents."
  "Title: "
  "#+TITLE: " str "\n"
  "#+AUTHOR: " user-full-name "\n"
  "#+DESCRIPTION: " (skeleton-read "Description: ") "\n"
  (let ((setupfiles-dir "~/Documents/org-files/Setupfiles/"))
    (format "#+SETUPFILE: %s\n"
             (read-file-name "Setupfile: " setupfiles-dir)))
  "#+bibliography: \"~/Documents/org-files/My Library.bib\"\n"
  (when (y-or-n-p "Auto-tangle? ")
    "#+auto_tangle: vars:org-babel-library-of-babel\n")
  "\n")

(define-skeleton skeleton-named-source-block
  "Insert a named source block in org mode."
  "Name: "
  "#+NAME: " str "\n"
  "#+begin_src " (skeleton-read "Mode: ") "\n"
  _ "\n"
  "#+end_src\n")

(define-skeleton skeleton-org-inline-src
  "Skeleton to create inline src blocks."
  "Mode: "
  "src_" str "[:exports code]{" _ "}")

#!/usr/bin/env bash
# packages.sh - script to list packages in init.

(grep -B 1 ':ensure nil' ~/.emacs.d/init.el | awk '/^\(use-package/ {print $2}';
awk '/^\(use-package/ {print $2}' ~/.emacs.d/init.el) \
| sort | uniq -u

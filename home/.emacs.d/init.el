;; Increasing garbage collection threshold for faster startup execution
(setq gc-cons-threshold (* 50 1000 1000))

(defun display-startup-time ()
  "Function to display startup time"
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'display-startup-time)

;; Silence compiler warnings as they can be pretty disruptive
(setq native-comp-async-report-warnings-errors nil)

(defun load-file-when-exists (file)
  "Load file when exists."
  (interactive)
  (when (file-exists-p file)
    (load-file file)))

;; Save custom variables externally
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load-file-when-exists custom-file)

(defvar-keymap my/map
  :doc "User-defined keybindings bound to \`C-c'."
  :parent (copy-keymap mode-specific-map)
  ;; comments
  "c b" #'comment-box
  "c c" #'comment-dwim
  "c e" #'comment-indent
  "c l" #'comment-line
  "c r" #'comment-region
  "c u" #'uncomment-region
  ;; misc
  "r" #'revert-buffer)

;; Set up package.el to work with MELPA, ORG, and ELPA
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(require 'use-package)

;; Always install packages if not already present
(setq use-package-always-ensure t)

(defadvice package-vc--unpack (around auto-confirm last compile activate)
  "Silence the `yes-or-no-p' function in `package-vc--unpack',
allowing use of `package-vc-install' in init."
  (cl-letf (((symbol-function 'yes-or-no-p) (lambda (&rest args) t)))
    ad-do-it))

(unless (package-installed-p 'vc-use-package)
  (package-vc-install "https://github.com/slotThe/vc-use-package"))

(defun my/package-vc-install (package &optional rev backend)
  "Install PACKAGE via `package-vc-install' if not already installed.
PACKAGE may either be a symbol or a cons-cell. `require' the
feature associated with the package's name, the `car' of SPEC."
  (let* ((tp (type-of package))
         (name (cond ((eq tp 'symbol) package)
                     ((eq tp 'cons) (car package))
                     (t (error "Invalid package type '%s'." tp)))))
    (unless (package-installed-p name)
      (package-vc-install package rev backend))
    (require name nil t)))

(setq inhibit-startup-message t)      ; remove default emacs startup message
(menu-bar-mode -1)                    ; remove menubar (file, edit, etc)
(tool-bar-mode -1)                    ; remove toolbar (save, quit, etc)
(scroll-bar-mode -1)                  ; remove scrollbar
(set-fringe-mode 10)                  ; left/right pixel margin

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))   ; one line at a time
      mouse-wheel-progressive-speed nil              ; don't accelerate scrolling
      mouse-wheel-follow-mouse 't                    ; scroll window under mouse
      scroll-step 1                                  ; keyboard scroll one line at a time
      ring-bell-function #'ignore)                   ; remove bell sound

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(defun set-font-faces ()
  "Sets default, fixed- and variable-pitch font faces."
  (message "Setting font faces.")
  (add-to-list 'default-frame-alist '(font . "Hack 12"))
  (set-face-attribute 'default nil :family "Hack" :height 120)
  (set-face-attribute 'fixed-pitch nil :font "Hack" :height 120 :weight 'regular)
  (set-face-attribute 'variable-pitch nil :font "Hack" :height 140 :weight 'regular)

  ;; Emojis
  (set-fontset-font
   t
   '(#x1f300 . #x1fad0)
   (cond
    ((member "Noto Color Emoji" (font-family-list)) "Noto Color Emoji")
    ((member "Symbola" (font-family-list)) "Symbola")))

  ;; JP
  (set-fontset-font
   t
   'han
   (cond
    ((member "Noto Sans CJK JP" (font-family-list)) "Noto Sans CJK JP")
    ((member "Noto Sans CJK TC" (font-family-list)) "Noto Sans CJK TC"))))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame
                  (set-font-faces))))
  (set-font-faces))

(require 'display-line-numbers)

(defcustom display-line-numbers-exempt-modes '(vterm-mode
                                               eshell-mode
                                               shell-mode
                                               term-mode
                                               ansi-term-mode
                                               inferior-python-mode
                                               org-mode
                                               treemacs-mode
                                               pdf-view-mode
                                               dashboard-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))

(column-number-mode)
(global-display-line-numbers-mode)        ; Display column numbers in modeline

;; Modeline configuration
(use-package doom-modeline
  :init
  (require 'nerd-icons)
  (unless (file-exists-p "~/.local/share/fonts/NFM.ttf")
    (nerd-icons-install-fonts t))
  (doom-modeline-mode 1))

;; Theme setting
(use-package doom-themes
  :init (load-theme 'doom-dracula t)
  :config
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  (doom-themes-org-config))

(defun untabify-all ()
  "Untabify entire file, unless `inhibit-untabify' is non-nil."
  (interactive)
  (unless inhibit-untabify
    (untabify (point-min) (point-max))))

(defun force-untabify-all ()
  "Force untabify files that have some read-only text."
  (interactive)
  (let ((inhibit-read-only t)) (untabify-all)))

(defvar inhibit-untabify nil
  "Buffer-local variable checked by `untabify-all'")

(setq-default tab-width 4)
(setq tab-always-indent 'complete)
(setq indent-tabs-mode nil)
(setq tab-stop-list (number-sequence tab-width 120 tab-width))
(setq evil-shift-width tab-width)          ; shift width for evil < and > keys
(add-hook 'before-save-hook 'untabify-all) ; convert tabs to spaces on save
(setq backward-delete-char-untabify-method nil)

;; Enable autofill mode in tex-mode
(add-hook 'tex-mode-hook 'auto-fill-mode)
(setq-default fill-column 80)

(defun comment-auto-fill ()
  (interactive)
  (setq-local comment-auto-fill-only-comments t)
  (auto-fill-mode 1))

;; Autofill comments automatically in programming modes
(add-hook 'prog-mode-hook 'comment-auto-fill)

(recentf-mode 1)                      ; save recent files across sessions
(setq recentf-max-menu-items 20       ; maximum displayed recent files
      recentf-max-saved-items 20)     ; maximum recent files saved across sessions

;; Create autosave directory unless it exists.
;; Autosaves don't automatically create parent directory.
(unless (file-directory-p (concat user-emacs-directory "autosaves"))
  (make-directory (concat user-emacs-directory "autosaves")))

(setq
  vc-make-backup-files t ; back up versioned files
  backup-by-copying t    ; don't clobber symlinks
  backup-directory-alist ; keep all backups in central folder
     `(("." . ,(concat user-emacs-directory "backups")))
  auto-save-file-name-transforms
     `((".*" "~/.emacs.d/autosaves/" t)) ; keep all autosaves in central folder
  delete-old-versions t  ; delete old backups
  kept-new-versions 6    ; number of new versions to keep
  kept-old-versions 2    ; number of old versions to keep
  version-control t)     ; version control backup names

(use-package dashboard
  :init
  (setq dashboard-startup-banner 'logo    ; display unofficial emacs logo
        dashboard-items '((recents . 10)  ; define the contents of dashboard
                          (projects . 5)
                          (bookmarks . 5)
                          (agenda . 5))
        dashboard-projects-backend 'project-el
        dashboard-projects-switch-function #'project-switch-project)
  :config
  (dashboard-setup-startup-hook)
  ;; set initial buffer to dashboard
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))

(use-package imenu-list
  :custom (imenu-list-position 'left)
  :config
  (keymap-global-set "C-'" #'imenu-list-smart-toggle))

(pixel-scroll-precision-mode 1)

(use-package edwina
  :custom (edwina-mfact 0.5)
  :hook (gnus-summary-mode
         .
         (lambda () (setq-local edwina-layout #'edwina-stack-layout)))
  :config
  (setq display-buffer-base-action '(display-buffer-below-selected))
  (edwina-mode 1))

(use-package evil
  :init
  (setq evil-want-keybinding nil
        evil-undo-system 'undo-fu         ; enable C-r redo functionality
        evil-want-Y-yank-to-eol t         ; same as nmap Y y$
        evil-want-C-u-delete t            ; C-u delete to indentation
        evil-want-C-w-delete t            ; C-w delete word backward
        evil-want-C-i-jump nil)           ; don't overwrite C-i (inserts tab)
  :config
  (evil-mode 1)                                                ; enable evil
  (keymap-set evil-insert-state-map "C-g" 'evil-normal-state)  ; C-g = ESC
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)     ; j go down visually
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line) ; k go up visually
  (evil-global-set-key 'motion "SPC" nil))                     ; unbind prefix key

(use-package evil-collection
  :after evil
  :init
  :config
  (dolist (mode '(tetris))
    (setq evil-collection-mode-list (delq mode evil-collection-mode-list)))
  (evil-collection-init))

(use-package evil-surround
  :config
  (global-evil-surround-mode 1))

(use-package vimish-fold
  :hook vimish-fold-mode-hook
  :config
  (require 'vimish-fold)
  (vimish-fold-global-mode 1)
  (keymap-set evil-normal-state-map "<TAB>" 'vimish-fold-toggle))

(use-package corfu
  ;; :hook (corfu-mode . corfu-popupinfo-mode)
  :custom
  (corfu-cycle t)                ; Enable cycling for `corfu-next/previous'
  (corfu-auto t)                 ; Enable auto completion
  (corfu-auto-prefix 2)          ; Complete after 2 characters
  (corfu-auto-delay 0.0)         ; Complete without delay
  (corfu-echo-documentation 0.25)       ; Echo docstrings after 0.25 sec
  :init
  (global-corfu-mode)
  (corfu-history-mode))

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(setq kind-icon-use-icons nil)
(setq kind-icon-mapping
      `((array ,(nerd-icons-codicon "nf-cod-symbol_array") :face font-lock-type-face)
        (boolean ,(nerd-icons-codicon "nf-cod-symbol_boolean") :face font-lock-builtin-face)
        (class ,(nerd-icons-codicon "nf-cod-symbol_class") :face font-lock-type-face)
        (color ,(nerd-icons-codicon "nf-cod-symbol_color") :face success)
        (command ,(nerd-icons-codicon "nf-cod-terminal") :face default)
        (constant ,(nerd-icons-codicon "nf-cod-symbol_constant") :face font-lock-constant-face)
        (constructor ,(nerd-icons-codicon "nf-cod-triangle_right") :face font-lock-function-name-face)
        (enummember ,(nerd-icons-codicon "nf-cod-symbol_enum_member") :face font-lock-builtin-face)
        (enum-member ,(nerd-icons-codicon "nf-cod-symbol_enum_member") :face font-lock-builtin-face)
        (enum ,(nerd-icons-codicon "nf-cod-symbol_enum") :face font-lock-builtin-face)
        (event ,(nerd-icons-codicon "nf-cod-symbol_event") :face font-lock-warning-face)
        (field ,(nerd-icons-codicon "nf-cod-symbol_field") :face font-lock-variable-name-face)
        (file ,(nerd-icons-codicon "nf-cod-symbol_file") :face font-lock-string-face)
        (folder ,(nerd-icons-codicon "nf-cod-folder") :face font-lock-doc-face)
        (interface ,(nerd-icons-codicon "nf-cod-symbol_interface") :face font-lock-type-face)
        (keyword ,(nerd-icons-codicon "nf-cod-symbol_keyword") :face font-lock-keyword-face)
        (macro ,(nerd-icons-codicon "nf-cod-symbol_misc") :face font-lock-keyword-face)
        (magic ,(nerd-icons-codicon "nf-cod-wand") :face font-lock-builtin-face)
        (method ,(nerd-icons-codicon "nf-cod-symbol_method") :face font-lock-function-name-face)
        (function ,(nerd-icons-codicon "nf-cod-symbol_method") :face font-lock-function-name-face)
        (module ,(nerd-icons-codicon "nf-cod-file_submodule") :face font-lock-preprocessor-face)
        (numeric ,(nerd-icons-codicon "nf-cod-symbol_numeric") :face font-lock-builtin-face)
        (operator ,(nerd-icons-codicon "nf-cod-symbol_operator") :face font-lock-comment-delimiter-face)
        (param ,(nerd-icons-codicon "nf-cod-symbol_parameter") :face default)
        (property ,(nerd-icons-codicon "nf-cod-symbol_property") :face font-lock-variable-name-face)
        (reference ,(nerd-icons-codicon "nf-cod-references") :face font-lock-variable-name-face)
        (snippet ,(nerd-icons-codicon "nf-cod-symbol_snippet") :face font-lock-string-face)
        (string ,(nerd-icons-codicon "nf-cod-symbol_string") :face font-lock-string-face)
        (struct ,(nerd-icons-codicon "nf-cod-symbol_structure") :face font-lock-variable-name-face)
        (text ,(nerd-icons-codicon "nf-cod-text_size") :face font-lock-doc-face)
        (typeparameter ,(nerd-icons-codicon "nf-cod-list_unordered") :face font-lock-type-face)
        (type-parameter ,(nerd-icons-codicon "nf-cod-list_unordered") :face font-lock-type-face)
        (unit ,(nerd-icons-codicon "nf-cod-symbol_ruler") :face font-lock-constant-face)
        (value ,(nerd-icons-codicon "nf-cod-symbol_field") :face font-lock-builtin-face)
        (variable ,(nerd-icons-codicon "nf-cod-symbol_variable") :face font-lock-variable-name-face)
        (t ,(nerd-icons-codicon "nf-cod-code") :face font-lock-warning-face)))

(electric-pair-mode)                  ; automatically insert matching delimiter
(show-paren-mode 1)                   ; highlight matching delimiter

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :bind                     ; remap emacs documentation functions to helpful
  ([remap describe-command] . helpful-command)
  ([remap describe-function] . helpful-function)
  ([remap describe-key] . helpful-key)
  ([remap describe-macro] . helpful-macro)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-variable] . helpful-variable))

(use-package vertico
  :bind (:map vertico-map
         ("C-f" . vertico-exit)
         ("C-<backspace>" . vertico-directory-delete-word))
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode))

(use-package savehist
  :ensure nil
  :init
  (setq history-length 25)
  (savehist-mode))

(use-package marginalia
  :after vertico
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package consult
  :bind (("C-s" . consult-line)
            ([remap switch-to-buffer] . consult-buffer)
         :map minibuffer-local-map
         ("C-r" . consult-history))
  :config
  (setq completion-in-region-function
        'consult-completion-in-region))

(use-package orderless
  :ensure t
  :custom (completion-styles '(orderless)))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode)

(use-package rainbow-mode
  :hook prog-mode)

(use-package embark
  :config
  (setq prefix-help-command #'embark-prefix-help-command)
  :bind
  ([remap describe-bindings] . #'embark-bindings)
  (:map my/map
        ("a a" . #'embark-act)
        ("a e" . #'embark-export)
        ("a ." . #'embark-dwim)))

(use-package cape
  :config
  (mapcar (lambda (x) (add-to-list 'completion-at-point-functions x))
          '(cape-file)))

(defun check-music (str)
  "Checks output of `dired-show-file-type' if file is music."
  (interactive)
  (if (string-match-p (regexp-quote "Audio") str)
      t
    nil))

(defun sudo-edit (&optional arg)
  "Uses ssh to edit read-only files via sudo."
  (interactive "P")
  (let ((fname (if (or arg (not buffer-file-name))
                   (read-file-name "File: ")
                 buffer-file-name)))
    (find-file
     (cond ((string-match-p "^/ssh:" fname)
            (with-temp-buffer
              (insert fname)
              (search-backward ":")
              (let ((last-match-end nil)
                    (last-ssh-hostname nil))
                (while (string-match "@\\\([^:|]+\\\)" fname last-match-end)
                  (setq last-ssh-hostname (or (match-string 1 fname)
                                              last-ssh-hostname))
                  (setq last-match-end (match-end 0)))
                (insert (format "|sudo:%s" (or last-ssh-hostname "localhost"))))
              (buffer-string)))
           (t (concat "/sudo:root@localhost:" fname))))))

(defun dired-open-file ()
  "Open file appropriately in dired."
  (interactive)
  (cond
   ;; Play music in emms
   ((check-music (dired-show-file-type (dired-get-file-for-visit)))
    (emms-play-file (dired-get-file-for-visit)))
   ;; All other files in emacs
   (t
    (dired-find-file))))

(defun my-file-find-hook ()
  "Enables checking for non-editable files and calls sudo-edit accordingly."
  (when (and (not (file-writable-p buffer-file-name))
             (y-or-n-p-with-timeout "File not writable. Open as root?" 5 nil))
    (let ((obuf (current-buffer)))
      (sudo-edit)
      (unless (equal (current-buffer) obuf)
        (let (kill-buffer-query-functions kill-buffer-hook)
          (kill-buffer obuf))))))

(add-hook 'find-file-hook 'my-file-find-hook)
(require 'evil-collection)
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :custom (dired-listing-switches "-agho --group-directories-first") ; specifies ls switches
  :config
  (define-key dired-mode-map (kbd " ") #'ignore t) ; undefine prefix key
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-up-directory    ; h goes up a directory
    "l" 'dired-open-file              ; l opens the file under the point
    "f" 'find-file                    ; f jumps to file (or creates it if nonexistent)
    " " nil)                          ; undefine prefix key
  (setq dired-auto-revert-buffer t    ; automatically redisplay buffer when revisiting
        dired-kill-when-opening-new-dired-buffer t))

(use-package nerd-icons-dired
  :hook (dired-mode . nerd-icons-dired-mode)) ; enable filetype icons in dired buffers

(defun ansi-term-zsh ()
  "Open ansi-term with zsh shell."
  (interactive)
  (ansi-term "/usr/bin/zsh"))

(keymap-set my/map "t a" #'ansi-term-zsh)

;; User bin directories (prefix with ~/)
(setq user-paths
      '("~/bin/"
        "~/.bin/"
        "~/.local/bin/"))

(defun existing-dirs (dirs)
  "Return a list with the only the existing directories in list DIRS."
  (let ((dirs dirs)                    ; copy DIRS
        (existing-dirs nil))           ; populate EXISTING-DIRS, from empty list
    (while dirs
      (let ((dir (pop dirs)))
        (when (file-directory-p dir)
          (setq existing-dirs (cons dir existing-dirs)))))
    existing-dirs))

;; Upon loading eshell, add existing dirs in USER-PATHS to the eshell path
(add-hook 'eshell-mode-hook
          (lambda ()
            (apply 'eshell/addpath
                   (mapcar 'expand-file-name
                           (existing-dirs user-paths)))))

(defun configure-eshell ()
  "Eshell configuration."
  ;; Minimal corfu interference
  (setq-local corfu-auto nil)
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'consult-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (keymap-set eshell-mode-map "TAB" 'completion-at-point) ; Vertico integration
  (evil-normalize-keymaps)
  (setq eshell-history-size         10000    ; maximum saved history
        eshell-buffer-maximum-lines 10000    ; maximum buffer scrollback
        eshell-hist-ignoredups t))           ; ignore duplicates

(use-package eshell-git-prompt
  :after eshell)

(use-package eshell
  :hook (eshell-first-time-mode . configure-eshell)
  :config
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)      ; kill buffer on exit
    (setq eshell-visual-commands '("htop" "zsh" "vim")))  ; enable htop, zsh, and vim
  (eshell-git-prompt-use-theme 'powerline)
  :bind (:map my/map ("t e" . #'eshell)))

(defun term-zsh ()
  "Open term with zsh shell."
  (interactive)
  (term "/usr/bin/zsh"))

(use-package term
  :ensure nil
  :commands term
  :config
  (setq terminal-prompt-regexp "[~\/A-Za-z-.@ ]*[%\$]")
  :bind (:map my/map ("t t" . #'term-zsh)))

(use-package vterm
  :ensure nil
  :commands vterm
  :init
  (setq vterm-always-compile-module t)    ; always compile module without prompt
  :config
  (setq vterm-kill-buffer-on-exit t)      ; kill buffer on exit
  :bind (:map my/map ("t v" . #'vterm)))

(keymap-set my/map "t s" #'shell)

(use-package breadcrumb
  :vc (breadcrumb
       :url "https://github.com/joaotavora/breadcrumb.git")
  :hook (eglot-managed-mode . breadcrumb-local-mode))

;; shellcheck for bash scripting
(use-package flycheck
  :config
  (add-hook 'sh-mode-hook 'flycheck-mode))

(setq-default c-basic-offset 4)
(with-eval-after-load 'lsp-mode
  (add-hook 'c-mode-hook 'eglot-ensure)
  (add-hook 'c++-mode-hook 'eglot-ensure)
  (require 'dap-cpptools))

;; Indentation settings
(put 'lambda 'lisp-indent-function 'defun)
(put 'while 'lisp-indent-function 1)
(put 'if 'lisp-indent-function 2)
(put 'when 'lisp-indent-function 1)
(put 'unless 'lisp-indent-function 1)
(put 'do 'lisp-indent-function 2)
(put 'do* 'lisp-indent-function 2)

(use-package slime
  :defer t
  :config
  (setq inferior-lisp-program (executable-find "sbcl"))
  (require 'slime)
  (slime-setup))

(autoload 'LilyPond-mode "lilypond-mode")
(setq auto-mode-alist                 ; automatically load lilypond-mode on *.ly files
      (cons '("\\.ly\\'" . LilyPond-mode) auto-mode-alist))
(setq auto-mode-alist                 ; automatically load lilypond-mode on *.ily files
      (cons '("\\.ily\\'" . LilyPond-mode) auto-mode-alist))
(add-hook 'LilyPond-mode-hook (lambda () (turn-on-font-lock)))

(use-package tuareg
  :mode ("\\.ml\\'" . tuareg-mode)
  :hook (tuareg-mode . eglot-ensure)
  )

(use-package merlin
  :hook (tuareg-mode . merlin-mode))

(use-package python-mode
  :mode "\\.py\\'"
  :hook (python-mode . eglot-ensure)
  :custom
  (python-shell-interpreter "python3"))

(use-package pyvenv
  :defer t
  :hook (python-mode . pyvenv-mode)
  :config
  ;; Restart the python process when switching environments
  (add-hook 'pyvenv-post-activate-hooks #'pyvenv-restart-python)
  ;; Use IPython when available or fall back to regular Python
  (cond
   ((executable-find "ipython")
    (setq python-shell-buffer-name "IPython"
          python-shell-interpreter "ipython"
          python-shell-interpreter-args "-i --simple-prompt"))
   ((executable-find "python3")
    (setq python-shell-interpreter "python3"))
   (t
    (setq python-shell-interpreter "python"))))

;; hook for rustic mode
(defun rustic-mode-setup ()
  "Function to setup rustic mode."
  (interactive)
  (eglot-ensure)
  (when buffer-file-name
    (setq-local buffer-save-without-query t)))

;; Setup rust mode
(use-package rustic
  :mode ("\\.rs\\'" . rustic-mode)
  :hook (rustic-mode . rustic-mode-setup)
  :config
  (setq rustic-format-on-save t))

(use-package mips-mode :mode "\\.mips\\'")

(use-package nix-mode
  :hook (nix-mode . eglot-ensure)
  :config
  (setq nix-nixfmt-bin "nixpkgs-fmt")
  (add-hook 'nix-mode-hook
            (lambda ()
              (when (eq major-mode #'nix-mode)
                (add-hook 'before-save-hook #'nix-format-before-save nil t)))))

;; don't untabify makefiles
(add-hook 'makefile-mode-hook (lambda () (setq-local inhibit-untabify t)))

(use-package poly-R
  :config
  (add-to-list 'auto-mode-alist '("\\.[rR]md\\'" . poly-gfm+r-mode))
  (setq markdown-code-block-braces t))

(use-package magit
  :diminish auto-revert
  :commands magit-status
  :custom
  ;; display in same buffer
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(setq vc-follow-symlinks t) ; follow version-controlled symlinks without prompt

(defun project-magit-status ()
  "Run `magit-status' in the current project's root."
  (interactive)
  (magit-status (project-root (project-current t))))

(with-eval-after-load 'project
  (add-to-list
   'project-switch-commands
   '(?m "Magit" project-magit-status)
   t))

(defun org-font-setup ()
  "Sets up fonts for org mode."
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (eval-expression (set-face-attribute (car face) nil :font "Hack" :weight 'regular :height (cdr face))))
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  (eval-expression (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch))
  (eval-expression (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch)))
  (eval-expression (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch)))
  (eval-expression (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch)))
  (eval-expression (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch)))
  (eval-expression (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch)))
  (eval-expression (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)))

(setq my-org-agenda-files '())

(defun add-org-agenda-file (file)
  "Function to add file to org agenda if it exists."
  (if (file-exists-p file)
      (setq my-org-agenda-files
            (add-to-list 'my-org-agenda-files file))))

;; Add org agenda files from predefined list
(mapcar 'add-org-agenda-file
        '("~/Documents/org-files/Tasks.org"
          "~/Documents/org-files/Habits.org"
          "~/Documents/org-files/Birthdays.org"
          "~/Documents/org-files/Events.org"))

(defun tangle-all-configs (&optional vc-file)
  "Tangles all org files in the git repo containing current file.
Unless specified, VC-FILE is used to search for .git folders at
the root of the repository."
  (interactive)
  (let* ((repo-root "~/.dotfiles/")
         (configs (split-string
                   (shell-command-to-string
                    (format "find %s -type f -iname \"*.org\""
                            repo-root)))))
    (mapcar #'org-babel-tangle-file configs)))

;; Functions to generate random org name strings
(defun random-string (length)
  "Generates a random string."
  (let ((str ""))
    (dotimes (i length)
      (let* ((charset "0123456789abcdef")
             (max (- (length charset) 1))
             (index (random max))
             (char (substring charset index (+ index 1))))
        (setq str (concat str char))))
    str))

(defun org-name-set-hash (arg &optional interactive?)
  "Inserts an org name keword line with a random, unique name.
ARG and INTERACTIVE? are interactive arguments and are passed to
`org-store-link'."
  (interactive "P\np")
  (if (when (boundp 'evil-mode) t)
      (evil-move-end-of-line)
    (move-end-of-line))
  (insert (if use-hard-newlines hard-newline "\n"))
  (insert "#+NAME: ")
  (let ((names (cdr (org-collect-keywords '("NAME")))))
    (let ((name (random-string 8)))
      (while (member name names)
        (setq name (random-string 8)))
      (insert name)
      (org-store-link arg interactive?))))

(use-package org
  :ensure nil
  :hook
  (org-mode . org-indent-mode)
  (org-mode . flyspell-mode)
  :commands (org-capture org-agenda)
  :custom
  (org-agenda-files my-org-agenda-files)
  (org-latex-caption-above '(table image))
  (org-latex-pdf-process '("tectonic %F"))
  :config
  (require 'ox-beamer)
  (setq org-src-preserve-indentation t)   ; respect indentation in src-blocks
  ;; Org Agenda
  (setq org-agenda-start-with-log-mode t
        org-log-done 'time                ; insert timestamp when agenda item done
        org-log-into-drawer t             ; log into attribute drawer
        org-refile-targets                ; files into which to archive old
                                          ; agenda items
        '((nil :level . 1) ("~/Documents/org-files/Archive.org" :maxlevel . 1)))
  (advice-add 'org-refile :after 'org-save-all-org-buffers) ; save org buffers
                                                            ; after refile
  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)   ; enable habit tracking
  ;; Org Babel
  (setq org-src-fontify-natively t        ; source block syntax highlighting
        org-confirm-babel-evaluate nil)   ; don't confirm evaluate
  ;; General
  (setq org-return-follows-link t         ; follow link on RET
        org-ellipsis " ▾"                 ; elipsis character to dropdown triangle
        org-hide-emphasis-markers t       ; hide font style characters
        org-clock-sound "~/.emacs.d/bell.wav")
  (if (daemonp)                           ; set org fonts properly if daemon mode
      (add-hook 'after-make-frame-functions
                (lambda (frame)
                  (with-selected-frame frame
                    (org-font-setup))))
    (org-font-setup))
  (setq org-latex-compiler "lualatex"
        org-preview-latex-default-process 'dvisvgm)
  (setq org-clock-persist 'history)       ; clock history persist across sessions
  (org-clock-persistence-insinuate)
  (mapcar (lambda (hook) (add-hook hook #'org-save-all-org-buffers))
          '(org-clock-in-hook org-clock-out-hook org-clock-cancel-hook)))

(use-package evil-org
  :after org
  :hook (org-mode . evil-org-mode)
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●"))) ; set custom bullet characters

(use-package org-appear
  :hook (org-mode . org-appear-mode)
  :config (setq org-appear-autolinks t))

(use-package org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :init
  (setq org-auto-tangle-babel-safelist
        (split-string
         (shell-command-to-string (format "find %s -type f"
                                          "~/.dotfiles/org/")))))

;; For csl-style citations
(use-package citeproc
  :after org
  :hook (org-mode . (lambda () (require 'oc-csl)))
  :config (setq org-cite-csl-styles-dir "~/Zotero/styles"))

(use-package org-contrib
  :after org
  :config
  (require 'ox-extra)
  (ox-extras-activate '(ignore-headlines)))

(use-package org-contacts
  :after org
  :init
  (setq org-contacts-files '("~/Documents/org-files/Contacts.org"))
  :config
  ;; this should force rebuild `org-contacts-db' and avoid errors when
  ;; completing contacts after startup
  (org-contacts-db))

(use-package org-alert
  :after org
  :config
  (setq alert-default-style 'libnotify
        org-alert-interval 600
        org-alert-notify-cutoff 30
        org-alert-notify-after-event-cutoff 10
        ;org-alert-match-string nil
        org-alert-time-match-string
        "\\(?:SCHEDULED:\\|DEADLINE:\\)?.*<.*\\([0-9]\\{2\\}:[0-9]\\{2\\}\\).*>")
  (org-alert-enable))

(use-package denote
  :config
  (setq denote-directory (expand-file-name "~/Documents/Notes/")
        denote-known-keywords '("emacs" "music"))
  (unless (file-directory-p denote-directory)
    (mkdir denote-directory))
  (add-hook 'dired-mode-hook #'denote-dired-mode)
  :bind (:map my/map
              ("n l" . #'denote-link-or-create)
              ("n n" . #'denote)
              ("n o" . #'denote-open-or-create)))

(use-package org-mime
  :bind (:map org-mode-map
         ("C-c M-o" . #'org-mime-org-buffer-htmlize)
         :map message-mode-map
         ("C-c M-o" . #'org-mime-htmlize)))

;; Define org capture templates
(setq org-capture-templates
      '(("t" "Task" entry (file "~/Documents/org-files/Tasks.org")
         "* TODO %^{task name}"
         :empty-lines 1
         :refile-targets (("~/Documents/org-files/Tasks.org" :maxlevel . 1)))
        ("e" "Event" entry (file "~/Documents/org-files/Events.org")
         "* %^{event name}: %^{event location}\n%(org-time-stamp t)"
         :empty-lines 1
         :refile-targets (("~/Documents/org-files/Events.org" :maxlevel . 2)))
        ("c" "Contact" entry (file "~/Documents/org-files/Contacts.org")
         "* %(org-contacts-template-name)
:PROPERTIES:
:ADDRESS: %^{Address}
:BIRTHDAY: %^{yyyy-mm-dd}
:EMAIL: %(org-contacts-template-email)
:NOTE: %^{NOTE}
:END:"
         :empty-lines 1)))

(setq org-src-languages
      '(C
        emacs-lisp
        java
        haskell
        lisp
        lua
        python
        shell
        haskell
        lilypond))

(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   (mapcar (lambda (x) (cons x t))
           org-src-languages)))

(with-eval-after-load 'org
  (require 'org-tempo)
  (mapcar (lambda (x) (add-to-list 'org-structure-template-alist x))
          '(("cc" . "src c")
            ("conf" . "src conf")
            ("el" . "src emacs-lisp")
            ("java" . "src java")
            ("hs" . "src haskell")
            ("lisp" . "src lisp")
            ("lua" . "src lua")
            ("ly" . "src lilypond")
            ("md" . "src markdown")
            ("ml" . "src ocaml")
            ("mips" . "src mips")
            ("nix" . "src nix")
            ("py" . "src python")
            ("sh" . "src shell")
            ("txt" . "src text"))))

(keymap-set my/map "o a" #'org-agenda)
(keymap-set my/map "o c" #'org-capture)
(keymap-set my/map "o d" #'skeleton-org-document)
(keymap-set my/map "o l" #'org-store-link)
(keymap-set my/map "o m" #'org-insert-macro-call)
(keymap-set my/map "o o" #'org-open-at-point)
(keymap-set my/map "o s" #'skeleton-org-inline-src)

(unless (package-installed-p 'auctex)
  (package-install 'auctex))
(require 'latex)

(setq TeX-engine-alist '((default
                           "Tectonic"
                           "tectonic -X compile -f plain %T"
                           "tectonic -X watch"
                           nil))
      LaTeX-command-style '(("" "%(latex)"))
      TeX-check-TeX nil
      TeX-process-asynchronous t
      TeX-engine 'default
      TeX-auto-save t
      TeX-parse-self t)
(setq-default TeX-master nil)
(let ((tex-list (assoc "TeX" TeX-command-list))
      (latex-list (assoc "LaTeX" TeX-command-list)))
  (setf (cadr tex-list) "%(tex)"
        (cadr latex-list) "%l"))

(load-file-when-exists "~/.gnus.el")

(keymap-set my/map "m g" #'gnus)
(keymap-set my/map "m m" #'my/compose-mail)
(keymap-global-set "C-x m" #'my/compose-mail)
(keymap-set my/map "m d" #'mimedown)

(use-package elfeed
  :config
  (setq elfeed-feeds
        '(("https://systemcrafters.net/rss/news.xml" emacs linux guix)
          ("https://blog.benoitj.ca/posts/index.xml" emacs linux nix guix)
          ("https://shom.dev/index.xml" emacs linux)
          ("https://planet.emacslife.com/atom.xml" emacs)
          ("https://xkcd.com/rss.xml" comic)
          ("https://www.smbc-comics.com/comic/rss" comic)
          ("https://www.cutcommonmag.com/feed" music)
          ("https://sequenza21.com/feed" music)
          ("https://www.icareifyoulisten.com/feed" music)
          ("https://emma.pet/emma.rss" cat)
          ("https://richarddavis.xyz/en/blog/rss.xml" music emacs linux nix)
          ("https://www.marxist.com/feed/rss/" news politics marxism)
          ("https://socialistrevolution.org/feed/" news politics marxism)))
  :bind (:map my/map
              ("e f" . #'elfeed)
              ("e u" . #'elfeed-update)))

(use-package elfeed-tube
  :after elfeed
  :config (elfeed-tube-setup)
  :bind (:map elfeed-show-mode-map
        ("F" . elfeed-tube-fetch)
        ([remap save-buffer] . elfeed-tube-save)
        :map elfeed-search-mode-map
        ("F" . elfeed-tube-fetch)
        ([remap save-buffer] . elfeed-tube-save)))

(use-package elfeed-tube-mpv
  :bind (:map elfeed-show-mode-map
              ("C-c C-f" . elfeed-tube-mpv-follow-mode)
              ("C-c C-w" . elfeed-tube-mpv-where)))

(setq subscriptions
      '("https://www.youtube.com/feeds/videos.xml?channel_id=UCQuGtU9yfkgh1xZ0vOx0Nhg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC7YOGHUfC1Tb6E4pudI9STA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCsCyncBPEzI6pb_pmALJ9Tw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCTUtqcDkzw7bisadh6AOx5w"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UChzJTbxs5kTvxgAklhCwh3A"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCTfabOKD7Yty6sDF4POBVqA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCiWsgUNiND0za5YWQQ2akrw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCV3h2_srSVaiEmjrZ8v7Qsw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC9MwazSehbDJe-eZ0BNdn-w"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCO_aKKYxn4tvrqPjcTzZ6EQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC_sFNM0z0MWm9A6WlKPuMMg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCmbs8T6MWqUHP1tIQvSgKrg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC5UAwBUum7CPN5buc-_N1Fw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC9p_lqQ0FEDz327Vgf5JwqA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCxQKHvKbmSzGMvUrVtJYnUA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCg02KL1G0nmH0QzFuLsVoRQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCgmPnx-EEeOrZSg5Tiw7ZRQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCJFZiqLMntJufDCHc6bQixg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCyzelLPcSrGUdLhN79eA4mg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCAiiOTio8Yu69c3XnR7nQBQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCKEt0RCO7UYiMiclJ7MI7Vw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCPdFDFTd6P1a__tAr8CrpCQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCXWwyi_B0gZKXZapfXj2Zog"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCo_Moc4DU_WbHpA3q5SKoSQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCKwZmQw3suiDykYW3evzzeQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCotXwY6s8pWmuWd_snKYjhg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCWrHUQo0T5lot1etA1gBMpA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCUqr0wbW78xM7OSBrAoXOgg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC3n5uGu18FoCy23ggWWp8tA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCoTPqHRmleube2CTt9xyodg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCraTIScYf9Ch5IqRhuY4IBQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCoSrY_IQQVpmIRZ9Xf-y93g"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC8Ig76wK-TrXzBc2YD1BjGw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCUfvTx9XCHqPlw5mRAiwntw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC758B56bESyznwmUP0o9FHA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCCzUftO8KOVkV4wQG1vkUvg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCFLCpXMBGf3wmPCKrPY_Luw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC96_aPfamLt7FDQB1QY7dOA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCVXuVbZjmXJ-1Bq18eb9XJQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCI1aF4MNqSzKIS2t0KHS1gw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCE_hjYd8HzRTVVXEr7jjHng"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCgnfPPb9JI3e9A4cXHnWbyg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCPdwE21gqS7voKPI2GDs_-A"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCwmBIysOtf85Bp-9Ibg3PVw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCL_qhgtOy0dy1Agp8vkySQg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC06F8EJQInq-O5SMaKeQJng"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCZkBfgFnWZdr2Kcsgcjba4Q"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCHsx4Hqa-1ORjQTh9TYDhww"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCvzGlP9oQwU--Y0r9id_jnA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCIfAvpeIWGHb0duCkMkmm2Q"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCHt7_vg0S5-wCIhvTWPt8Dw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCJetJ7nDNLlEzDLXv7KIo0w"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCVEfsQRmGsCpeldR7zSfwTQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC8THb_fnOptyVgpi3xuCd-A"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCDZJSQjCs-pBkUpAUgq8tfA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC9wbdkwvYVSgKtOZ3Oov98g"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCt9H_RpQzhxzlyBxFqrdHqA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCeWDDbfQxKv0Cgq_UNpwYpA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC2PA-AKmVpU6NKCGtZq_rKQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC5fdssPqmmGhkhsJi4VcckA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC8rcEBzJSleTkf_-agPM20g"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCRe1bmHSfA3rqh9BkqQD3XA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCK0efN6mG-J8gv9Qq-MHGPA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCTUHzVzRwN_2x13IWQ9QVNg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCUkkNJ4opr29yrnFqKqzdCg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCvtY3MPYXQp9aZBt0gQD7-w"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCWrjEwCdDhXSA45V97RjBSw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC8S-QUgU-FgtQBI8gux0rtQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCG8n6v0rtM5WDd3MKMJFYsg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCdwYq-TV00WE_GaDP6rdm4g"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCu6ElsWTHPd6N3IwtgihZhA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCJXBx07zlcEMmSEHSxhUb1A"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCyl1z3jo3XHR1riLFKG5UAg"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCSpHQW__TTUG7UqIF0Fn5xQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC_gQM0LTi8Z2dsgKpHgG75g"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCJSnyk3A7FnYxsYKH-gVkKw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCmsh8m3Db38NDM_OOggUt8A"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC041NK81P_KJdzg8oYyV4Rw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC9usUc8xKXr0x3shJET172w"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC3vIimi9q4AT8EgxYp_dWIw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCGSOfFtVCTBfmGxHK5OD8ag"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCH_w7kdposi2X3wgd--EDlA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCCIHOP7e271SIumQgyl6XBQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCS2U0CjmYhwXzYU31STkIgQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC63OdqYTFmDk90vftUouF_Q"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UChFljs1bkJ_T-gh5ThWu7sA"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC-H-ILUy0JR5e-RJ1-wduxQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCF2fAIp1x-1Gp_GXmhHJRfQ"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCeHKrRJyGpn5CpEIh420f3w"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC9ruVYPv7yJmV0Rh0NKA-Lw"
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCSiCOed6SRB33UoNg0O-Eig"  
        ))

(setq elfeed-feeds
      (append elfeed-feeds
              (mapcar (lambda (x) `(,x youtube)) subscriptions)))

(setq browse-url-browser-function 'eww-browse-url) ; follow links in eww
(with-eval-after-load 'shr                         ; word wrapping
  (setq shr-width 80
        shr-use-fonts nil
        shr-use-colors nil))

(use-package erc-at-nick
  :vc (erc-at-nick
       :url "https://github.com/purplg/erc-at-nick.git")
  :config (add-to-list 'erc-modules 'at-nick)
  ;; basic completion styles so nicknames match from the beginning
  :hook (erc-mode . (lambda () (setq-local completion-styles '(basic)))))

(use-package erc-hl-nicks
  :after erc
  :config (add-to-list 'erc-modules 'hl-nicks))

(use-package erc-image
  :after erc
  :config
  (setq erc-image-inline-rescale 300)
  (add-to-list 'erc-modules 'image))

(use-package emojify
  :hook (erc-mode . emojify-mode)
  :commands emojify-mode)

(defun my/erc ()
  "Run erc with my configuration"
  (interactive)
  (erc-tls
   :user erc-nick
   :password (auth-source-pass-get 'secret
                                   (format "%s/%s"
                                           erc-server
                                           erc-nick))))

(use-package erc
  :config
  (add-to-list 'erc-modules 'sasl)
  (add-to-list 'erc-modules 'notifications)
  (erc-update-modules)
  :bind (:map my/map ("i" . #'my/erc))
  :custom
  (erc-sasl-mechanism 'plain)
  (erc-sasl-auth-source-function #'erc-auth-source-search)
  (erc-server "irc.libera.chat")
  (erc-port 6697)
  (erc-nick "davisrichard437")
  (erc-user-full-name user-full-name)
  (erc-email-userid user-mail-address)
  (erc-autojoin-channels-alist '(("Libera.Chat"
                                  "#emacs"
                                  "#systemcrafters"
                                  "#nixos")))
  (erc-kill-buffer-on-part t)
  (erc-kill-queries-on-quit t)
  (erc-kill-server-buffer-on-quit t)
  (erc-prompt-for-password nil)
  (erc-track-exclude-types '("JOIN" "NICK" "QUIT" "MODE" "AWAY"))
  (erc-hide-list '("JOIN" "NICK" "QUIT" "MODE" "AWAY")))

(use-package 0x0
  :bind (:map my/map
              ("p p" . #'0x0-dwim)
              ("p t" . #'0x0-upload-text)
              ("p f" . #'0x0-upload-file)
              ("p k" . #'0x0-upload-kill-ring)))

(defun track-title-from-file-name (file)
  "For using with EMMS description functions. Extracts the track
title from the file name FILE, which just means a) taking only
the file component at the end of the path, and b) removing any
file extension."
  (with-temp-buffer
    (save-excursion (insert (file-name-nondirectory (directory-file-name file))))
    (ignore-error 'search-failed
      (search-forward-regexp (rx "." (+ alnum) eol))
      (delete-region (match-beginning 0) (match-end 0)))
    (buffer-string)))

(defun my-emms-track-description (track)
  "Return a description of TRACK, for EMMS, but uses
`track-title-from-file-name' to remove directory and extension
from filename in the abscence of metadata information."
  (let ((artist (emms-track-get track 'info-artist))
        (title (emms-track-get track 'info-title)))
    (cond ((and artist title)
           (concat artist " - " title))
          (title title)
          ((eq (emms-track-type track) 'file)
           (track-title-from-file-name (emms-track-name track)))
          (t (emms-track-simple-description track)))))

;; Set up EMMS
(use-package emms
  :commands (emms
             emms-add-file
             emms-add-directory
             emms-add-dired
             emms-play-file
             emms-play-directory
             emms-play-dired)
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)
  ;; Media keys to control emms playback
  (keymap-global-set "<XF86AudioPlay>" 'emms-pause)
  (keymap-global-set "<XF86AudioStop>" 'emms-stop)
  (keymap-global-set "<XF86AudioPrev>" 'emms-previous)
  (keymap-global-set "<XF86AudioNext>" 'emms-next)
  ;; wayland media keys?
  (keymap-global-set "<AudioPlay>" 'emms-pause)
  (keymap-global-set "<AudioStop>" 'emms-stop)
  (keymap-global-set "<AudioPrev>" 'emms-previous)
  (keymap-global-set "<AudioNext>" 'emms-next)
  (setq
   emms-source-file-default-directory "~/Music/"
   emms-repeat-playlist t
   emms-random-playlist t
   emms-track-description-function 'my-emms-track-description))

(use-package pdf-tools
  :ensure nil
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :config
  (unless (file-executable-p pdf-info-epdfinfo-program)
    (pdf-tools-install t)))

(use-package writegood-mode
  :hook (flyspell-mode . writegood-mode))

(require 'ispell)
(add-to-list 'ispell-local-dictionary-alist '("en_US"
                                              "[[:alpha:]]"
                                              "[^[:alpha:]]"
                                              "[']"
                                              t
                                              ("-d" "en_US")
                                              nil
                                              iso-8859-1))

(setq ispell-program-name "hunspell" ; Use hunspell to correct mistakes
      ispell-dictionary   "en_US")   ; Default dictionary to use

(autoload 'ispell-get-word "ispell")

(defun lookup-word (word)
  (interactive (list (save-excursion (car (ispell-get-word nil)))))
  (browse-url (format "https://www.webster-dictionary.org/definition/%s" word)))

(keymap-global-set "M-#" 'lookup-word)

(use-package ledger-mode
  :mode ("\\.dat\\'" . ledger-mode)
  :config
  (setq ledger-reports
        (mapcar (lambda (l) (list (car l) (concat (cadr l) " --strict")))
                ledger-reports)))

(defun lookup-password (&rest keys)
  "Look up password in authinfo file according to plist KEYS."
  (let ((result (apply #'auth-source-search keys)))
    (if result
        (funcall (plist-get (car result) :secret))
      nil)))

(defun org-collect-keyword (keyword)
  "Return a list of values associated with the single org KEYWORD."
  (cdr (assoc keyword (org-collect-keywords (list keyword)))))

(defun org-get-buffer-defined-macros (buffer)
  "Return a list of names of macros defined in BUFFER."
  (with-current-buffer buffer
    (org-collect-keyword "MACRO")))

(defun org-get-defined-macros ()
  "Return a list of all macros defined in the current buffer.
This includes those in INCLUDE files or SETUPFILEs, as well as some of the
defaults described in Info node `(org) Macro Replacement'."
  (append '("title"
            "author"
            "email"
            "date"
            "time"
            "modification-time"
            "input-file")
          (mapcan #'org-get-buffer-defined-macros
                  `(,(current-buffer)
                    ,@(mapcar #'find-file-noselect
                              (org-collect-keyword "SETUPFILE"))
                    ,@(mapcar #'find-file-noselect
                              (org-collect-keyword "INCLUDE"))))))

(defun re-seq (regexp string)
  "Get a list of all regexp matches in a string"
  (save-match-data
    (let ((pos 0)
          matches)
      (while (string-match regexp string pos)
        (push (match-string 0 string) matches)
        (setq pos (match-end 0)))
      (reverse matches))))

(defun org-insert-macro-call (&optional macro &rest args)
  "Insert a call to MACRO with arguments ARGS in an org-mode buffer at point."
  (interactive)
  (let* ((macros (org-get-defined-macros))
         (macro
          (or macro
              (completing-read "Macro: " macros)))
         (args
          (or args
              (mapcar
               (lambda (x)
                 (read-string
                  (format "%s in %s: " x
                          (string-join (cdr (split-string macro)) " "))))
               (re-seq "\$[0-9]" macro)))))
    (insert "{{{" (nth 0 (split-string macro))
            (if args                    ; needs to be IF to return a string
                (format
                 "(%s)"
                 (string-join
                  (mapcar (lambda (s)
                            (replace-regexp-in-string
                             ","
                             "\,"
                             (replace-regexp-in-string
                              "\\,"
                              "\\," s nil t) nil t))
                          args)
                  ","))
              "")
            "}}}")))

;; List of packages not otherwise installed.
(defvar package-list '(diminish
                       ess
                       geiser-guile
                       haskell-mode
                       lua-mode
                       markdown-mode
                       password-store
                       scratch
                       sed-mode
                       simple-httpd
                       undo-fu
                       vimrc-mode
                       yaml-mode)
  "List of packages to be installed.")

;; Add all packages in use-package statements
;; Except those with :ensure nil, already installed with emacs
(setq package-list
      (append package-list
              (mapcar 'intern
                      (split-string
                       (shell-command-to-string
                        (concat user-emacs-directory "packages.sh"))))))

;; Install packages from `package-list'
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; load skeletons.el (templates for portions of files)
(load-file-when-exists "~/.emacs.d/skeletons.el")

;; word wrapping
(add-hook 'text-mode-hook 'visual-line-mode)     ; word wrapping in plaintext mode
(add-hook 'markdown-mode-hook 'visual-line-mode) ; word wrapping in markdown mode

(use-package visual-fill-column
  :hook
  (pdf-view-mode . visual-fill-column-mode)
  (visual-line-mode . visual-fill-column-mode)
  (visual-line-mode
   .
   (lambda ()
     (when line-number-mode
       (setq-local visual-fill-column-extra-text-width '(6 . 0)))))
  :custom
  (visual-fill-column-center-text t))

;; Save place when opening files
(save-place-mode)

;; Automatically revert files when changed on disk
(global-auto-revert-mode)

;; Load saved kbd macros
(setq saved-macros-file
      (concat user-emacs-directory
              "saved-macros.el"))

(load-file-when-exists saved-macros-file)

;; Functions to save macros
(defun read-symbol-name (prompt)
  "Read a symbol name until arriving at a valid symbol."
  (let* ((s (read-from-minibuffer prompt))
         (sym (intern s)))
    (if (not (fboundp sym))
        sym ; return sym if all good
      (progn
        (message "Symbol name %s already defined." s)
        (sleep-for 2)
        (read-symbol-name prompt)))))

(defun save-macro ()
  "Save the current macro to file."
  (interactive)
  (let ((macroname (read-symbol-name "Macro name: "))
        (oldbuf (current-buffer)))
    (kmacro-name-last-macro macroname)
    (with-current-buffer (find-file-noselect saved-macros-file)
      (end-of-buffer)
      (insert "\n")
      (insert-kbd-macro macroname)
      (save-buffer))))

;; Fix performance issues in buffers with long lines.
(global-so-long-mode)

(defun edit-config (&optional dir)
  "Edit config in DIR, defaulting to the org subdirectory of the repository root."
  (interactive)
  (find-file (read-file-name "Config: "
                             (or dir "~/.dotfiles/org/"))))

(defun edit-emacs ()
  "Edit emacs configuration."
  (interactive)
  (find-file "~/.dotfiles/org/emacs.org")
  (org-overview)
  (beginning-of-buffer))

(keymap-set my/map "e c" #'edit-config)
(keymap-set my/map "e e" #'edit-emacs)

(defun menu-music-add-directory ()
  "Add user-chosen music directory."
  (interactive)
  (emms-add-directory-tree (read-directory-name
                            "Add music directory: "
                            emms-source-file-default-directory)))

(defun menu-music-add-file ()
  "Add user-chosen music file."
  (interactive)
  (emms-add-file (read-file-name
                  "Add music file: "
                  emms-source-file-default-directory)))

(defun menu-music-play-directory ()
  "Play user-chosen music directory."
  (interactive)
  (emms-play-directory-tree (read-directory-name
                             "Play music directory: "
                             emms-source-file-default-directory)))

(defun menu-music-play-file ()
  "Play user-chosen music file."
  (interactive)
  (emms-play-file (read-file-name
                   "Play music file: "
                   emms-source-file-default-directory)))

(use-package hydra
  :defer t)

(defhydra hydra-zoom (:timeout 4)
  "Scale text"
  ("j" zoom-in "zoom in")
  ("k" zoom-out "zoom out")
  ("f" nil "finished" :exit t))

(defhydra hydra-window-resize (:timeout 4)
  "Resize windows"
  ("l" enlarge-window-horizontally "enlarge window horizontally")
  ("h" shrink-window-horizontally "shrink window horizontally")
  ("j" enlarge-window "enlarge window")
  ("k" shrink-window "shrink window")
  ("f" nil "finished" :exit t))

(defhydra hydra-window-move (:timeout 4)
  "Move windows"
  ("l" evil-window-move-far-right "move window right")
  ("h" evil-window-move-far-left "hove window left")
  ("j" evil-window-move-very-bottom "move window down")
  ("k" evil-window-move-very-top "move window up")
  ("f" nil "finished" :exit t))

(defhydra hydra-org-todo (:timeout 4)
  "Toggle org todo items"
  ("t" org-todo "toggle todo state")
  ("f" nil "finished" :exit t))

(keymap-global-set "<escape>" 'keyboard-escape-quit) ; Bind escape to quit
(keymap-global-set "C--" #'(lambda () (interactive) (text-scale-increase -1))) ; Zoom out
(keymap-global-set "C-=" #'(lambda () (interactive) (text-scale-increase 1))) ; Zoom in

(keymap-global-set "C-c" my/map)

;; (require 'server)
;; (unless (server-running-p)
;;   (server-start))

;; Set garbage collection back to lower, but maintainable, threshold for normal operation
(setq gc-cons-threshold (* 2 1000 1000))

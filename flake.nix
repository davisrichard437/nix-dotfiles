{
  description =
    "Richard Davis's system configurations, including nixos and home-manager.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixgl = {
      url = "github:guibou/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    playlists = {
      url = "gitlab:davisrichard437/playlists";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  nixConfig = {
    auto-optimise-store = true;
    keep-derivations = false;
    keep-outputs = false;
    sandbox = "relaxed";
  };

  outputs = { nixpkgs, home-manager, nixgl, playlists, ... }:
    let

      system = "x86_64-linux";

      electronWaylandOverlay = prgm: (self: super: {
        ${prgm} = super.${prgm}.overrideAttrs (old: {
          preFixup = old.preFixup + ''
            gappsWrapperArgs+=(
            --add-flags "enable-features=UseOzonePlatform"
            --add-flags "--ozone-platform=wayland"
                                                )
          '';
        });
      });

      lib = nixpkgs.lib;

      pkgs = { wayland }: import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
        overlays = [
          nixgl.overlay
          playlists.overlays.default
        ] ++ lib.optional wayland (electronWaylandOverlay "signal-desktop");
      };

    in
    {
      nixosConfigurations = {
        nixos-laptop = lib.nixosSystem {
          inherit system;
          pkgs = pkgs { wayland = true; };

          modules = [
            ./systems/nixos-laptop/configuration.nix
            home-manager.nixosModules.home-manager
          ];
        };
      };

      homeConfigurations =
        let
          hc = { laptop, nixOS, ... }:
            home-manager.lib.homeManagerConfiguration {
              pkgs = pkgs { wayland = laptop; };
              modules = [
                ./systems/common/home.nix
              ];
              extraSpecialArgs = {
                inherit laptop nixOS;
              };
            };
        in
        {
          nixos-laptop = hc {
            laptop = true;
            nixOS = true;
          };
          nixos-desktop = hc {
            laptop = false;
            nixOS = true;
          };
          non-nixos-laptop = hc {
            laptop = true;
            nixOS = false;
          };
          non-nixos-desktop = hc {
            laptop = false;
            nixOS = false;
          };
        };

      packages."${system}" =
        import ./mypkgs { nixpkgs = nixpkgs.legacyPackages."${system}"; };
    };
}

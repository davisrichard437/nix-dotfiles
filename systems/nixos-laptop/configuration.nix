# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  hardware = {
    enableAllFirmware = true;
    bluetooth.enable = true;
    graphics = {
      enable = true;
      enable32Bit = true;
    };
    cpu.intel.updateMicrocode = true;
    cpu.amd.updateMicrocode = true;
    firmware = [ ];
  };

  services.fwupd.enable = true;

  # automatically generate man caches
  documentation.man.generateCaches = true;

  networking.hostName = "nixos-laptop"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  programs.dconf.enable = true;

  programs.udevil.enable = true;

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
    localNetworkGameTransfers.openFirewall = true;
    gamescopeSession.enable = true;
  };

  services.xserver.videoDrivers = [ "amdgpu" ];

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is
    # enabled by default, no need to redefine it in your config for now)
    # media-session.enable = true;
  };

  services.blueman.enable = true;

  # Define a user account. Don't forget to set a password with 'passwd'.
  users.users.richard = {
    isNormalUser = true;
    description = "Richard Davis";
    extraGroups = [ "networkmanager" "wheel" "input" ];
    initialPassword = "password";
    packages = with pkgs; [ firefox zsh ];
    shell = "${pkgs.zsh}/bin/zsh";
  };
  home-manager.users.richard =
    (import ../common/home.nix)
      {
        inherit config pkgs;
        laptop = true;
        nixOS = true;
      };

  # Allow unfree packages
  # nixpkgs.config.allowUnfree = true;
  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    auto-optimise-store = true;
    keep-outputs = false;
    keep-derivations = false;
    trusted-users = [ "root" "@wheel" ];
  };

  nix.gc = {
    automatic = true;
    dates = "weekly";
  };

  # List packages installed in system profile. To search, run:
  # $ nix search nixpkgs wget
  environment.systemPackages = with pkgs; [
    # Do not forget to add an editor to edit configuration.nix! The Nano editor
    # is also installed by default.
    acpi
    cacert
    file
    glibc
    glibcInfo
    # gsettings-desktop-schemas
    killall
    libnotify
    man-pages
    man-pages-posix
    pciutils
    usbutils
    vim
    wget
  ];

  environment.pathsToLink = [ "/share/zsh" ];

  # enable location service (required for redshift)
  services.geoclue2 = {
    enable = true;
    appConfig.redshift = {
      isAllowed = true;
      isSystem = true;
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.avahi = {
    enable = true;
    openFirewall = true;
    nssmdns4 = true;
  };

  services.tlp.enable = true;

  services.thermald.enable = true;

  services.devmon.enable = true;

  services.soju.enable = true;
  services.soju.listen = [ "irc+insecure://127.0.0.1:6667" ];

  # vbox setup
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "richard" ];

  security.polkit.enable = true;
  security.pam.services.swaylock = {}; # swaylock
  services.dbus = {
    enable = true;
    packages = [ pkgs.gcr ];    # gnome3 pinentry
  };
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    config.common.default = "*";
  };

  # programs.regreet.enable = true;
  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = "sway > .cache/sway.log";
        user = "richard";
      };
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}

{ config
, pkgs
, laptop # flags to fine-tune configuration
, nixOS
, ...
}:

let

  # my own packages
  mypkgs = import ../../mypkgs { nixpkgs = pkgs; };

  # nixpkgs library functions
  lib = pkgs.lib;

  # latex setup for emacs org mode
  tex = (pkgs.texlive.combine {
    inherit (pkgs.texlive)
      # for preview and export as html
      scheme-full dvisvgm dvipng
      wrapfig amsmath ulem hyperref capt-of;
  });

  myEmacs = with pkgs;
    ((emacsPackagesFor (if nixOS then emacs29-pgtk else emacs29)).emacsWithPackages
      (import ../common/emacs-deps.nix));

  myPython =
    let
      python =
        let
          packageOverrides = self: super: {
            opencv4 = super.opencv4.override {
              enableGtk2 = true;
              gtk2 = pkgs.gtk2;
            };
          };
        in
        pkgs.python3.override { inherit packageOverrides; self = python; };
    in
    python.withPackages (ps: with ps; [
      # abjad
      build
      flake8-bugbear
      flake8-docstrings
      flake8-import-order
      ipython
      kneed
      matplotlib
      mypy
      natsort
      numpy
      opencv4
      pandas
      pillow
      pip
      pygments
      pyls-flake8
      pylsp-mypy
      python-lsp-ruff
      python-lsp-server
      pyqt5
      scikit-image
      scipy
      toolz
      tqdm
      types-pillow
      universal-pathlib
      yt-dlp
    ]);

  autostartApps = with pkgs; [
    {
      drv = vesktop;
      exec = "vesktop";
    }
    {
      drv = signal-desktop;
      exec = "signal-desktop";
    }
    {
      drv = thunderbird;
      exec = "thunderbird";
    }
  ];

  mkAutostartDesktop =
    { drv, exec }: rec {
      name = "${exec}.desktop";
      value = {
        executable = true;
        recursive = false;
        target = "autostart/${name}";
        source = "${drv}/share/applications/${exec}.desktop";
      };
    };

in
{

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "richard";
  home.homeDirectory = "/home/richard";

  # set target to generic linux, enabling optimizations for non-nixos linux
  targets.genericLinux.enable = !nixOS;

  nix = {
    package = if nixOS then lib.mkOptionDefault pkgs.nix else pkgs.nix;
    settings = {
      accept-flake-config = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  home.packages = (with pkgs; [
    ani-cli
    arandr
    audacity
    bat
    brightnessctl
    cargo
    clang-tools
    corefonts
    curl
    davmail
    dwarf-fortress-packages.dwarf-fortress-full
    eza
    feh
    ffmpeg
    fiji
    fira-code-nerdfont
    gcc
    gdb
    gimp
    gmrun
    gnumake
    gnupg
    gnutls
    (gscreenshot.override { x11Support = false; })
    gucharmap
    guile
    hack-font
    htop
    hunspell
    hunspellDicts.en-us
    imagemagick
    ipafont
    jdk11
    # katawa-shoujo
    kdenlive
    ledger
    liberation_ttf
    libinput-gestures
    libreoffice-fresh
    libsvm
    lightlocker
    lilypond-unstable-with-fonts
    linuxsampler
    mpv
    musescore
    myEmacs
    myPython
    ncdu
    # neofetch
    nixd
    nixpkgs-fmt
    noto-fonts
    noto-fonts-emoji
    opam
    pamixer
    pass
    pavucontrol
    pcmanfm
    pdftk
    pyright
    qsampler
    reaper
    remmina
    ripgrep
    rlwrap
    rust-analyzer
    rustc
    rustfmt
    sbcl
    shellcheck
    slack
    sway-audio-idle-inhibit
    swaybg
    system-config-printer
    tectonic
    tex
    tidal-hifi
    transmission_4-gtk
    tree
    ubuntu_font_family
    unzip
    vlc
    wdisplays
    wget
    wildmidi
    xclip
    xdgmenumaker
    xmenu
    xournalpp
    yad
    zip
    zotero
  ])
  ++ (with pkgs.mate; [ atril engrampa eom mate-calc ])
  ++ (with mypkgs; [ mimedown words xclickroot xsudo ])
  ++ (with pkgs.xorg; [ xev xkill ])
  ++ (lib.optionals (!laptop) [ pkgs.xdotool mypkgs.dracula-openbox-theme ])
  ++ (lib.optionals nixOS (with pkgs; [ zoom-us grim slurp wl-clipboard ]))
  ++ (map (builtins.getAttr "drv") autostartApps);

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  i18n.inputMethod = {
    enabled = "fcitx5";
    fcitx5.addons = with pkgs; [ fcitx5-configtool fcitx5-mozc ];
  };

  fonts.fontconfig.enable = true;

  gtk = {
    enable = true;
    cursorTheme = {
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
    };
    font = {
      name = "Noto Sans 11";
      package = pkgs.noto-fonts;
    };
    iconTheme = {
      name = "Tela-purple-dark";
      package = pkgs.tela-icon-theme;
    };
    theme = {
      name = "Dracula";
      package = pkgs.dracula-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "gtk";
  };

  xresources.properties = {
    "Xft.dpi" = 96;
    "xmenu.font" = "xft:Hack Mono:regular:size=12";
    "xmenu.background" = "#282a36";
    "xmenu.foreground" = "#f8f8f2";
    "xmenu.selbackground" = "#bd93f9";
    "xmenu.selforeground" = "#f8f8f2";
    "xmenu.border" = "#282a36";
    "xmenu.separator" = "#44475a";
    "xmenu.gap" = "6";
    "xmenu.width" = "80";
    "xmenu.height" = "24";
    "xmenu.borderWidth" = "2";
    "xmenu.separatorWidth" = "1";
    "xmenu.alignment" = "left";
    "Xcursor.theme" = "Adwaita";
  };

  home.pointerCursor = {
    name = "Adwaita";
    package = pkgs.adwaita-icon-theme;
    size = 16;
    gtk.enable = true;
    x11.enable = true;
  };

  programs.firefox = {
    enable = true;
    profiles.default = {
      name = "default";
      isDefault = true;
      search = {
        default = "DuckDuckGo";
        force = true;
      };
    };
  };

  programs.i3status = {
    enable = true;
    general = {
      colors = true;
      color_good = "#50fa7b";
      color_degraded = "#f1fa8c";
      color_bad = "#ff5555";
    };
  };

  programs.alacritty = {
    enable = true;
    package =
      if nixOS then
        pkgs.alacritty
      else
        mypkgs.alacritty-nixgl;
    settings = import ../../nix/alacritty.nix;
  };

  programs.git = {
    enable = true;
    package = pkgs.gitFull;
    userName = "Richard Davis";
    userEmail = "davisrichard437@gmail.com";
    extraConfig = {
      init.defaultBranch = "main";
      credential.helper = "store";
    };
  };

  programs.vim = {
    enable = true;
    extraConfig = builtins.readFile ../../extra/extra.vimrc;
    plugins = with pkgs.vimPlugins; [ nerdtree surround ];
  };

  programs.bash = {
    enable = true;
    shellAliases = (import ../../nix/aliases.nix) { inherit laptop nixOS; };
    bashrcExtra = builtins.readFile ../../extra/extra.bashrc;
    profileExtra = builtins.readFile ../../extra/extra.profile;
  };

  programs.zsh = {
    enable = true;
    autocd = true;
    defaultKeymap = "emacs";
    autosuggestion.enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    initExtra = builtins.readFile ../../extra/extra.zshrc;
    shellAliases = (import ../../nix/aliases.nix) { inherit laptop nixOS; };
  };

  # programs.ssh.enable = true;

  programs.rofi = {
    enable = true;
    font = "Hack 13";
    pass.enable = true;
    terminal = "${pkgs.alacritty}/bin/alacritty";
    theme = ../../extra/dracula.rasi;
  };

  programs.tint2 = {
    enable = !laptop;
    extraConfig = builtins.readFile ../../extra/extra.tint2rc;
  };

  programs.wofi.enable = true;

  programs.waybar = {
    enable = true;

    settings = {
      mainBar = {
        layer = "top";
        position = "top";
        output = [
          "eDP-1"
          "DP-3"
        ];
        modules-left = [ "sway/mode" "sway/workspaces" "sway/window" ];
        modules-center = [ ];
        modules-right = [ "tray" ]
                        # only include battery widget if on a laptop
                        ++ (lib.optional laptop "battery")
                        ++ [ "custom/wttr" "clock" ];

        "battery" = {
          format = "{capacity}% {icon} ";
          format-icons = [ "" "" "" "" "" ];
          states = {
            warning = 30;
            critical = 15;
          };
        };
        "clock" = {
          format = "{:%Y-%m-%d %a, %H:%M}";
        };
        "sway/mode".format = "{}";
        "sway/window".format = "{}";
        "sway/workspaces" = {
          all-outputs = true;
        };
        "tray" = {
          spacing = 6;
        };
        "custom/wttr" = {
          format = "{}";
          exec = ".local/bin/wttr";
          restart-interval = 60;
        };
      };
    };

    style = ''
      * {
        border: none;
        border-radius: 0;
        font-family: Hack;
        font-size: 12px;
        min-height: 24px;
      }

      window#waybar {
        background: transparent;
      }

      window#waybar.hidden {
        opacity: 0.2;
      }

      #workspaces {
        margin-top: 4px;
        margin-left: 4px;
        margin-right: 4px;
        margin-bottom: 0;
        border-radius: 26px;
        background: #282a36;
        transition: none;
      }

      #workspaces button {
        transition: none;
        color: #f8f8f2;
        background: transparent;
        font-size: 16px;
      }

      #workspaces button.focused {
        color: #bd93f9;
      }

      #workspaces button.urgent {
        color: #ff5555;
      }

      #workspaces button:hover {
        transition: none;
        box-shadow: inherit;
        text-shadow: inherit;
        color: #ff79c6;
      }

      window#waybar.empty {
        background-color: transparent;
      }

      window#waybar.empty #window {
        padding: 0px;
        margin: 0px;
        border: 0px;
        background-color: transparent;
      }

      #mode,
      #battery,
      #window,
      #tray,
      #custom-wttr,
      #clock {
        margin-top: 4px;
        margin-left: 4px;
        margin-right: 4px;
        padding-left: 16px;
        padding-right: 16px;
        margin-bottom: 0;
        border-radius: 26px;
        transition: none;
        color: #f8f8f2;
        background: #282a36;
      }

      #battery.warning {
        color: #f1fa8c;
      }

      #battery.critical {
        color: #ff5555;
      }
    '';
  };

  programs.swaylock = {
    enable = true;
    package = pkgs.swaylock-effects;
    settings = {
      color = "282a36";
      clock = true;
      datestr = "%Y-%m-%d %a";
      fade-in = 0.2;
      screenshots = true;
      effect-blur = "7x5";
      font = "Hack";
      indicator-radius = 100;
      indicator-thickness = 7;
      line-color = "f8f8f2";
      ring-color = "bd93f9";
      ring-caps-lock-color = "f1fa8c";
      ring-wrong-color = "ff5555";
      ring-clear-color = "ffb86c";
      ring-ver-color = "8be9fd";
      separator-color = "f8f8f2";
      inside-color = "282a36";
      inside-clear-color = "282a36";
      inside-wrong-color = "ff5555";
      inside-ver-color = "ff79c6";
      key-hl-color = "8be9fd";
      bs-hl-color = "ff5555";
      text-color = "f8f8f2";
      text-clear-color = "f8f8f2";
      text-caps-lock-color = "f8f8f2";
      text-ver-color = "f8f8f2";
      text-wrong-color = "f8f8f2";
    };
  };

  home.file = {
    ".emacs.d" = {
      source = ../../home/.emacs.d;
      recursive = true;
    };
    ".gnus.el".source = ../../home/.gnus.el;
    ".local" = {
      source = ../../home/.local;
      recursive = true;
    };
    "Wallpapers" = {
      source = ../../home/Wallpapers;
      recursive = true;
    };
    "Music/JPop" = {
      source = pkgs.playlists.jpop;
      recursive = true;
    };
    "Music/Punk Pop" = {
      source = pkgs.playlists.punkpop;
      recursive = true;
    };
  };

  xdg = {
    enable = true;

    configHome = "/home/richard/.config";
    configFile =
      let
        relativeConfigLocation = ../../home/.config;
      in
      (builtins.mapAttrs
        (name: value:
          {
            source = relativeConfigLocation + "/${name}";
            recursive = value == "directory";
          })
        ((builtins.readDir relativeConfigLocation)));

    desktopEntries = {

      emacsClient = {
        name = "Emacs (Client)";
        genericName = "Text Editor";
        comment = "Edit text";
        mimeType = [
          "text/english"
          "text/plain"
          "text/x-makefile"
          "text/x-c++hdr"
          "text/x-c++src"
          "text/x-chdr"
          "text/x-csrc"
          "text/x-java"
          "text/x-moc"
          "text/x-pascal"
          "text/x-tcl"
          "text/x-tex"
          "application/x-shellscript"
          "text/x-c"
          "text/x-c++"
        ];
        exec = "emacsclient -c -a \"emacs\" %F";
        icon = "emacs";
        type = "Application";
        terminal = false;
        categories = [ "Development" "TextEditor" "Utility" ];
      };

      lilypondInvokeEditor = {
        name = "lilypond-invoke-editor";
        genericName = "Textedit URI handler";
        comment = "URI handler for textedit:";
        exec = "lilypond-invoke-editor %u";
        terminal = false;
        type = "Application";
        mimeType = [ "x-scheme-handler/textedit" ];
        categories = [ "TextEditor" ];
        noDisplay = true;
      };

    };

  };

  services.blueman-applet.enable = true;

  services.cbatticon = {
    enable = laptop;            # only enable if a laptop
    commandCriticalLevel = "notify-send --urgency=critical 'Battery critical!'";
    criticalLevelPercent = 5;
    lowLevelPercent = 30;
    iconType = "standard";
    updateIntervalSeconds = 30;
  };

  services.dunst = {
    enable = true;
    iconTheme = {
      name = "Tela-purple-dark";
      package = pkgs.tela-icon-theme;
    };
    settings = import ../../nix/dunst.nix;
  };

  services.emacs = {
    enable = true;
    client = {
      enable = true;
      arguments = [ "-c" "-a" "emacs" ];
    };
    defaultEditor = true;
    startWithUserSession = true;
    package = myEmacs;
  };

  services.gpg-agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-gnome3;
  };

  services.kanshi = {
    enable = true;
    settings =
      let
        x = 2256;
        y = 1504;
        ref = 59.999;
        scale = 1.5;
        disp = {
          criteria = "eDP-1";
          inherit scale;
          mode = with builtins; "${toString x}x${toString y}@${toString ref}Hz";
          position = "0,0";
        };
      in
        [
          {
            output = {
              criteria = "eDP-1";
              inherit scale;
            };
          }
          {
            profile.name = "default";
            profile.outputs = [
              disp
            ];
          }
          {
            profile.name = "connected";
            profile.outputs = [
              {
                criteria = "DP-3";
                position = with builtins; "0,0";
              }
              (disp // { position = "1920,0"; })
            ];
          }
        ];
  };

  services.mpris-proxy.enable = true;

  services.network-manager-applet.enable = true;

  services.parcellite.enable = true;

  services.picom = {
    enable = true;
    activeOpacity = 1;
    inactiveOpacity = 1;
    menuOpacity = 1;
    # override picom config location to the one that is correctly generated?
    # extraArgs = [ "--config ~/.config/picom/picom.conf" ];
    fade = true;
    fadeDelta = 10; # ms
    fadeSteps = [ 0.1 0.1 ];
    settings = { shadow-radius = 7; };
    shadow = true;
    shadowExclude = [
      "name = 'Notification'"
      "class_g ?= 'Notify-osd'"
      "name = 'Plank'"
      "name = 'Docky'"
      "name = 'Kupfer'"
      "name = 'xfce4-notifyd'"
      "name *= 'VLC'"
      "name *= 'compton'"
      "name *= 'Chromium'"
      "name *= 'Chrome'"
      "class_g = 'Firefox' && argb"
      "class_g = 'dzen'"
      "class_g = 'Conky'"
      "class_g = 'Kupfer'"
      "class_g = 'Synapse'"
      "class_g ?= 'Notify-osd'"
      "class_g ?= 'Cairo-dock'"
      "class_g = 'Cairo-clock'"
      "class_g ?= 'Xfce4-notifyd'"
      "class_g ?= 'Xfce4-power-manager'"
      "_GTK_FRAME_EXTENTS@:c"
    ];
    shadowOffsets = [ (0 - 7) (0 - 7) ];
    shadowOpacity = 0.75;
    vSync = true;
  };

  services.redshift = {
    enable = true;
    provider = "geoclue2";
    temperature = {
      day = 6500;
      night = 4800;
    };
    settings = {
      redshift = {
        brightness-day = 1;
        brightness-night = 0.6;
        adjustment-method = "randr";
      };
    };
  };

  services.swayidle = {
    enable = true;
    events =
      let
        sw = "${pkgs.swaylock-effects}/bin/swaylock --daemonize";
      in [
        { event = "before-sleep"; command = sw; }
        { event = "lock"; command = sw; }
      ];
    timeouts = [
      {
        timeout = 120;
        command = "${pkgs.brightnessctl}/bin/brightnessctl --save s 1%";
        resumeCommand = "${pkgs.brightnessctl}/bin/brightnessctl --restore";
      }
      {
        timeout = 240;
        command = "${pkgs.systemd}/bin/systemctl suspend";
        resumeCommand = "${pkgs.brightnessctl}/bin/brightnessctl --restore";
      }
    ];
  };

  services.udiskie.enable = true;

  services.xidlehook = {
    enable = true;
    detect-sleep = true;
    not-when-audio = true;
    timers = [
      {
        delay = 120;
        command = "${pkgs.brightnessctl}/bin/brightnessctl --save s 5%";
        canceller = "${pkgs.brightnessctl}/bin/brightnessctl --restore";
      }
      {
        delay = 120;
        command = "${pkgs.systemd}/bin/systemctl suspend";
        canceller = "${pkgs.brightnessctl}/bin/brightnessctl --restore";
      }
    ];
  };

  services.xsettingsd = {
    enable = true;
    settings = {
      "Gtk/CursorThemeName" = "Adwaita";
      "Net/IconThemeName" = "Tela-purple-dark";
      "Net/ThemeName" = "Dracula";
    };
  };

  home.sessionVariables =
    let
      makePluginPath = format:
        (lib.makeSearchPath format ([
          "$HOME/.nix-profile/lib"
        ]
        ++ lib.optionals nixOS
          [
            "/run/current-system/sw/lib"
            "/etc/profiles/per-user/$USER/lib"
          ]))
        + ":$HOME/.${format}"
        + ":${pkgs.linuxsampler}/lib/linuxsampler";
    in
    {
      # plugins
      DSSI_PATH = makePluginPath "dssi";
      LADSPA_PATH = makePluginPath "ladspa";
      LV2_PATH = makePluginPath "lv2";
      LXVST_PATH = makePluginPath "lxvst";
      VST_PATH = makePluginPath "vst";
      VST3_PATH = makePluginPath "vst3";
    } // {
      # others
      JAVA_HOME = "${pkgs.jdk11}/lib/openjdk";
      LYEDITOR = "emacs";
      QT_QPA_PLATFORM = if laptop then "wayland" else "xcb";
      WEKA_JAR_PATH = "${pkgs.weka}/share/weka/weka.jar";
    };

  systemd.user.targets.tray = if !nixOS then {
    Unit = {
      Description = "Home Manager System Tray";
      Requires = [ "graphical-session-pre.target" ];
    };
  } else { };

  xfconf.enable = nixOS;        # only enable on nixOS

  xsession = {
    enable = true;
    numlock.enable = true;
    profileExtra = builtins.readFile ../../extra/extra.xprofile;
    windowManager.command =
      if laptop then
        "sway" # sway for laptop
      else
        "${pkgs.openbox}/bin/openbox-session"; # openbox for desktop
  };

  wayland.windowManager.sway = {
    enable = true;

    config = rec {
      modifier = "Mod4";
      terminal = "alacritty";
      focus.wrapping = "yes";

      startup = [
        { command = "swaybg -i $(find -L $HOME/Wallpapers -type f | shuf -n1) -m fill"; }
        { command = "systemctl --user import-environment WAYLAND_DISPLAY"; }
        { command = "dbus-update-activation-environment WAYLAND_DISPLAY"; }
        { command = "nohup davmail .config/davmail.properties"; }
        { command = "sway-audio-idle-inhibit"; }
      ] ++ lib.optional laptop { command = "swaylock --daemonize";  };

      input = {
        "type:touchpad" = {
          tap = "enabled";
          natural_scroll = "enabled";
        };
        "type:keyboard" = {
          xkb_options = "ctrl:nocaps";
        };
      };

      output.eDP-1 = {
        scale = "1.5";
        mode = "2256x1504@59.999Hz";
      };

      keybindings = lib.mkOptionDefault {
        "${modifier}+Tab" = "layout toggle all";
        "${modifier}+b" = "exec firefox";
        "${modifier}+Shift+b" = "exec firefox --private-window";
        "${modifier}+e" = "exec emacsclient -c -a emacs";
        "${modifier}+Shift+e" = "exec emacs -Q";
        "${modifier}+f" = "exec pcmanfm";
        "${modifier}+Shift+f" = "fullscreen toggle";
        "${modifier}+d" = "exec wofi --show drun";
        "${modifier}+Shift+d" = "exec wofi --show run";
        "${modifier}+o" = "splith";
        "${modifier}+s" = "exec .local/bin/wofi-power-menu";
        "${modifier}+q" = "reload";
        "${modifier}+Shift+q" = "exit";
        "${modifier}+w" = "kill";
        Print = "exec grim";
        "Shift+Print" = "exec grim -g \"$(slurp)\"";
        "Mod1+Print" = "exec grim - | wl-copy";
        "Shift+Mod1+Print" = "exec grim -g \"$(slurp)\" - | wl-copy";
        XF86AudioRaiseVolume = "exec .local/bin/volume -i 5";
        XF86AudioLowerVolume = "exec .local/bin/volume -d 5";
        XF86AudioMute = "exec .local/bin/volume -t";
        XF86MonBrightnessUp = "exec .local/bin/brightness 5%+";
        XF86MonBrightnessDown = "exec .local/bin/brightness 5%-";
        "${modifier}+Mod1+l" = "workspace next";
        "${modifier}+Mod1+h" = "workspace prev";
      };

      colors = {
        background = "#282a36";
        focused = {
          background = "#bd93f9";
          border = "#bd93f9";
          childBorder = "#bd93f9";
          indicator = "#bd93f9";
          text = "#f8f8f2";
        };
        unfocused = {
          background = "#282a36";
          border = "#282a36";
          childBorder = "#282a36";
          indicator = "#282a36";
          text = "#f8f8f2";
        };
        urgent = {
          background = "#ff5555";
          border = "#ff5555";
          childBorder = "#ff5555";
          indicator = "#ff5555";
          text = "#f8f8f2";
        };
      };

      fonts = {
        names = [ "Hack" ];
        style = "Normal";
        size = 8.0;
      };

      bars = [
        { command = "waybar"; }
      ];

      gaps = let gap = 6; in {
        smartBorders = "on";
        horizontal = gap;
        vertical = gap;
        # outer = gap;
        inner = gap;
        right = gap;
        left = gap;
        bottom = gap;
        top = gap;
      };

      floating.criteria = [
        { app_id = ".gscreenshot-wrapped"; }
      ];

    };

  };

}

epkgs:

with epkgs;

[
  doom-modeline
  doom-themes
  nerd-icons
  dashboard
  imenu-list
  edwina
  evil
  evil-collection
  evil-surround
  vimish-fold
  corfu
  kind-icon
  helpful
  vertico
  marginalia
  consult
  orderless
  which-key
  rainbow-mode
  embark
  embark-consult
  cape
  nerd-icons-dired
  eshell-git-prompt
  vterm
  flycheck
  slime
  tuareg
  merlin
  python-mode
  pyvenv
  rustic
  mips-mode
  nix-mode
  reformatter
  poly-R
  magit
  evil-org
  org-bullets
  org-appear
  org-auto-tangle
  citeproc
  org-contrib
  org-contacts
  org-alert
  denote
  org-mime
  auctex
  elfeed
  elfeed-tube
  elfeed-tube-mpv
  erc-hl-nicks
  erc-image
  emojify
  epkgs."0x0"
  emms
  pdf-tools
  writegood-mode
  ledger-mode
  diminish
  ess
  geiser-guile
  haskell-mode
  lua-mode
  markdown-mode
  password-store
  scratch
  sed-mode
  simple-httpd
  undo-fu
  vimrc-mode
  yaml-mode
  visual-fill-column
  hydra
]

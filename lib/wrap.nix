{ pkgs, package, nixgl, nixglName, binary }:

let

  wrapped = pkgs.writeShellScriptBin "${binary}" ''
    # call ${binary} as wrapped by ${nixgl}
    exec ${nixgl}/bin/${nixglName} ${package}/bin/${binary} "$@"
  '';

in

pkgs.symlinkJoin {
  name = "${binary}";
  paths = [ wrapped package ];
}

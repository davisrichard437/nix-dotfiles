{ pkgs, ... }:

{
  wrap = import ./wrap.nix;

  adwLaunch = { pkgs, package, binary }:
    let
      wrapped = pkgs.writeShellScriptBin "${binary}" ''
        # call ${binary} in Adwaita GTK theme
        exec env GTK_THEME=Adwaita ${package}/bin/${binary} "$@"
      '';
    in
    pkgs.symlinkJoin {
      name = "binary";
      paths = [ wrapped package ];
    };

  callPackage =
    let
      callPackageWith = autoArgs: fn: args:
        let
          f = if builtins.isFunction fn then fn else import fn;
          fargs = builtins.functionArgs f;
          # Merge autoArgs, fargs, and args into allArgs, to be arguments to fn
          # intersect to make sure all necessary arguments are present
          allArgs = builtins.intersectAttrs fargs autoArgs // args;
        in
        f allArgs;
    in
    callPackageWith pkgs; # partially apply callPackageWith to members in pkgs
}

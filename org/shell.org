#+TITLE: Shell Configuration
#+AUTHOR: Richard Davis
#+DESCRIPTION: configuration for the various shells to be used in the nix-dotfiles system
#+startup: overview
#+auto_tangle: vars:org-babel-library-of-babel

* Bash

#+begin_src shell :tangle ../extra/extra.bashrc :mkdirp t :noweb yes
<<common>>

# command not found handler
command_not_found_handle() {
    printf '%s: command not found: %s\n' $(basename "$SHELL") "$1"
    read -p "Try with nix-shell? [Y/n] " -n 1 ns
    echo

    <<command_not_found>>
}

PS1='\[\e[32m\]\u\[\e[37m\]@\[\e[36m\]\h\[\e[37m\]\[\e[1;34m\] \w\[\e[0;33m\]`git_prompt`\[\e[37m\]\$ '

# shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

export HISTCONTROL=ignoreboth:erasedups
#+end_src

* ZSH

#+begin_src shell :tangle ../extra/extra.zshrc :mkdirp t :noweb yes
<<common>>

# command not found handler
command_not_found_handler() {
    printf '%s: command not found: %s\n' $(basename "$SHELL") "$1"
    read -k "ns?Try with nix-shell? [Y/n] "
    echo

    <<command_not_found>>
}

# zsh prompt enable
setopt prompt_subst
PROMPT='%F{green}%n%f@%F{magenta}%m%f %F{blue}%B%~%b%f%F{yellow}$(git_prompt)%f%# '

# Better history search
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY_TIME
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

# open command in $EDITOR
autoload edit-command-line
zle -N edit-command-line
bindkey '^X^e' edit-command-line

# Enable command correction
setopt CORRECT

# Keybindings
# https://zsh.sourceforge.io/Doc/Release/Zsh-Line-Editor.html#Zle-Builtins for functions
# $ showkey -a for key sequences
WORDCHARS='~!#$%^&*(){}[]<>?+;-'
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^H" backward-kill-word
bindkey "^[[3;5~" delete-word
bindkey "\e[3~" delete-char
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
#+end_src

* Common

** Aliases

A nix module that defines all of my shell aliases, used by home-manager for shell configuration.

#+begin_src nix :tangle ../nix/aliases.nix :mkdirp t :noweb yes
{ laptop, nixOS }:

{
  "l." = "eza -a --group-directories-first | egrep '^\\.'";
  cat = "bat";
  cleanup =
    if nixOS then
      "nix-collect-garbage -d"
    else
      "nix-collect-garbage -d; sudo pacman -Rns $(pacman -Qtdq)";
  df = "df -h -x squashfs -x tmpfs -x devtmpfs";
  du = "du -h";
  egrep = "egrep --color=auto";
  fgrep = "fgrep --color=auto";
  free = "free -mt";
  git-update = "git commit -am 'Minor updates.' && git push";
  grep = "grep --color=auto";
  hs =
    if laptop then
      "home-manager --flake <<repo-root>>#non-nixos-laptop switch -b backup"
    else
      "home-manager --flake <<repo-root>>#non-nixos-desktop switch -b backup";
  ix = "curl -F 'f:1=<-' ix.io";
  l = "eza --group-directories-first";
  la = "eza -lah --group-directories-first";
  ll = "eza -lah --group-directories-first";
  ls = "eza -l --group-directories-first";
  merge = "xrdb -merge $HOME/.Xresources";
  mpvc = "mpv \"$(wl-paste)\"";
  ns = "sudo nixos-rebuild switch --flake <<repo-root>>#";
  pacman = "pacman --color auto";
  pandoc =
    "pandoc -V margin-top=1in -V margin-left=1in -V margin-right=1in -V margin-bottom=1in -V papersize=letter";
  pkgcount = "pacman -Q | wc -l";
  psa = "ps auxf";
  psgrep = "ps aux | grep -v grep | grep -i -e VSZ -e";
  rf = "readlink -f";
  rg = "rg --sort path";
  sbcl = "rlwrap sbcl";
  sn = "shutdown now";
  speedtest =
    "curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -";
  sr = "reboot";
  su = "sudo -i";
  tree = "eza -T";
  update-fc = "sudo fc-cache -fv";
  update-grub = "sudo grub-mkconfig -o /boot/grub/grub.cfg";
  userlist = "cut -d: -f1 /etc/passwd";
  vb = "$EDITOR ~/.bashrc";
  vconfgrub = "sudo $EDITOR /boot/grub/grub.cfg";
  vfstab = "sudo $EDITOR /etc/fstab";
  vgrub = "sudo $EDITOR /etc/default/grub";
  vlightdm = "sudo $EDITOR /etc/lightdm/lightdm.conf";
  vmirrorlist = "sudo $EDITOR /etc/pacman.d/mirrorlist";
  vmkinitcpio = "sudo $EDITOR /etc/mkinitcpio.conf";
  vnsswitch = "sudo $EDITOR /etc/nsswitch.conf";
  vpacman = "sudo $EDITOR /etc/pacman.conf";
  vsamba = "sudo $EDITOR /etc/samba/smb.conf";
  vsddm = "sudo $EDITOR /etc/sddm.conf";
  vz = "$EDITOR ~/.zshrc";
  wttr = "curl wttr.in\\?m";
  ymd = "date +%F";
  yta-aac = "youtube-dl --extract-audio --audio-format aac ";
  yta-best = "youtube-dl --extract-audio --audio-format best ";
  yta-flac = "youtube-dl --extract-audio --audio-format flac ";
  yta-m4a = "youtube-dl --extract-audio --audio-format m4a ";
  yta-mp3 = "youtube-dl --extract-audio --audio-format mp3 ";
  yta-opus = "youtube-dl --extract-audio --audio-format opus ";
  yta-vorbis = "youtube-dl --extract-audio --audio-format vorbis ";
  yta-wav = "youtube-dl --extract-audio --audio-format wav ";
  ytv-best = "youtube-dl -f bestvideo+bestaudio ";
}
#+end_src

** Command Not Found

The following code handles "command not found" errors in both bash and zsh.

#+NAME: command_not_found
#+begin_src shell
case "$ns" in
    "n" | "N")
        return 127
        ;;
    ,*)
        echo "Trying via nix-shell..."
        cmd=$(printf 'nix-shell -p "%s" --run "%s"' "$1" "$*")
        eval "$cmd"
        rc=$?
        return $rc
        ;;
esac
#+end_src

** =git_prompt=

#+NAME: git_prompt
#+begin_src shell
git_prompt() {
    ref=$(git symbolic-ref HEAD 2> /dev/null | cut -d'/' -f3-)
    if [ ! -z "$ref" ] ; then
        echo " @ $ref "
    else
        echo " "
    fi
}
#+end_src

** =cw=

#+NAME: cw
#+begin_src shell
cw() {
    bin="$(which $1)" && bat "$bin"
}
#+end_src

** =ex=

#+NAME: ex
#+begin_src shell
ex() {
    if [[ -f $1 ]]; then
        case $1 in
            ,*.tar.bz2)   tar xvjf $1   ;;
            ,*.tar.gz)    tar xvzf $1   ;;
            ,*.bz2)       bunzip2 $1   ;;
            ,*.rar)       unrar x $1   ;;
            ,*.gz)        gunzip $1    ;;
            ,*.tar)       tar xvf $1    ;;
            ,*.tbz2)      tar xvjf $1   ;;
            ,*.tgz)       tar xvzf $1   ;;
            ,*.zip)       unzip $1     ;;
            ,*.Z)         uncompress $1;;
            ,*.7z)        7z x $1      ;;
            ,*.deb)       ar x $1      ;;
            ,*.tar.xz)    tar xvf $1    ;;
            ,*.tar.zst)   tar xvf $1    ;;
            ,*)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
#+end_src

** =rw=

#+NAME: rw
#+begin_src shell
rw() {
    readlink -f $(which $1)
}
#+end_src

** Adding paths

#+NAME: addpath
#+begin_src shell
for d in "bin" ".bin" ".local/bin"; do
    if [[ -d "$HOME/$d" ]]; then
        export PATH="$HOME/$d:$PATH"
    fi
done
#+end_src

** Editor

#+NAME: editor
#+begin_src shell
export EDITOR='vim'
export VISUAL='vim'
#+end_src

** Common

#+NAME: common
#+begin_src shell :noweb yes
# -*- mode: shell-script; -*-
#############
# FUNCTIONS #
#############

<<git_prompt>>

<<cw>>

<<ex>>

<<rw>>

#########
# PATHS #
#########

<<addpath>>

export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}

########
# MISC #
########

<<editor>>
#+end_src

* Local variables

Prevent files that must be encoded with tabs from being untabified, source org-babel variables.

# Local Variables:
# eval: (org-babel-lob-ingest "./variables.org")
# End:

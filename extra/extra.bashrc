# -*- mode: shell-script; -*-
#############
# FUNCTIONS #
#############

git_prompt() {
    ref=$(git symbolic-ref HEAD 2> /dev/null | cut -d'/' -f3-)
    if [ ! -z "$ref" ] ; then
        echo " @ $ref "
    else
        echo " "
    fi
}

cw() {
    bin="$(which $1)" && bat "$bin"
}

ex() {
    if [[ -f $1 ]]; then
        case $1 in
            *.tar.bz2)   tar xvjf $1   ;;
            *.tar.gz)    tar xvzf $1   ;;
            *.bz2)       bunzip2 $1   ;;
            *.rar)       unrar x $1   ;;
            *.gz)        gunzip $1    ;;
            *.tar)       tar xvf $1    ;;
            *.tbz2)      tar xvjf $1   ;;
            *.tgz)       tar xvzf $1   ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1;;
            *.7z)        7z x $1      ;;
            *.deb)       ar x $1      ;;
            *.tar.xz)    tar xvf $1    ;;
            *.tar.zst)   tar xvf $1    ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

rw() {
    readlink -f $(which $1)
}

#########
# PATHS #
#########

for d in "bin" ".bin" ".local/bin"; do
    if [[ -d "$HOME/$d" ]]; then
        export PATH="$HOME/$d:$PATH"
    fi
done

export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}

########
# MISC #
########

export EDITOR='vim'
export VISUAL='vim'

# command not found handler
command_not_found_handle() {
    printf '%s: command not found: %s\n' $(basename "$SHELL") "$1"
    read -p "Try with nix-shell? [Y/n] " -n 1 ns
    echo

    case "$ns" in
        "n" | "N")
            return 127
            ;;
        *)
            echo "Trying via nix-shell..."
            cmd=$(printf 'nix-shell -p "%s" --run "%s"' "$1" "$*")
            eval "$cmd"
            rc=$?
            return $rc
            ;;
    esac
}

PS1='\[\e[32m\]\u\[\e[37m\]@\[\e[36m\]\h\[\e[37m\]\[\e[1;34m\] \w\[\e[0;33m\]`git_prompt`\[\e[37m\]\$ '

# shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

export HISTCONTROL=ignoreboth:erasedups

" ~/.vimrc
" config file for the vim text editor

" Lilypond configuration
filetype off
set runtimepath+=/usr/share/vim/vimfiles/syntax/lilypond.vim
filetype on
syntax on

" Misc options
set ttymouse=sgr
set mouse=a
set number
set autoindent
set tabstop=4
set expandtab
set foldmethod=marker
set clipboard=unnamedplus

" Keybindings
nmap Y y$
nmap <Space>w <C-W>
imap <C-BS> <C-W>
imap <C-Del> <C-o>de

" Plugin installation for non-arch systems
" Run :PlugInstall on first boot to work
"call plug#begin()
"Plug 'tpope/vim-fugitive'
"Plug 'preservim/nerdtree'
"call plug#end()

" Exit if NERDTree is last buffer
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

func! WordProcessorMode()
    setlocal smartindent
    setlocal spell spelllang=en_us
    setlocal noexpandtab
endfu

com! WP call WordProcessorMode()

# -*- mode: shell-script; -*-
#############
# FUNCTIONS #
#############

git_prompt() {
    ref=$(git symbolic-ref HEAD 2> /dev/null | cut -d'/' -f3-)
    if [ ! -z "$ref" ] ; then
        echo " @ $ref "
    else
        echo " "
    fi
}

cw() {
    bin="$(which $1)" && bat "$bin"
}

ex() {
    if [[ -f $1 ]]; then
        case $1 in
            *.tar.bz2)   tar xvjf $1   ;;
            *.tar.gz)    tar xvzf $1   ;;
            *.bz2)       bunzip2 $1   ;;
            *.rar)       unrar x $1   ;;
            *.gz)        gunzip $1    ;;
            *.tar)       tar xvf $1    ;;
            *.tbz2)      tar xvjf $1   ;;
            *.tgz)       tar xvzf $1   ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1;;
            *.7z)        7z x $1      ;;
            *.deb)       ar x $1      ;;
            *.tar.xz)    tar xvf $1    ;;
            *.tar.zst)   tar xvf $1    ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

rw() {
    readlink -f $(which $1)
}

#########
# PATHS #
#########

for d in "bin" ".bin" ".local/bin"; do
    if [[ -d "$HOME/$d" ]]; then
        export PATH="$HOME/$d:$PATH"
    fi
done

export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}

########
# MISC #
########

export EDITOR='vim'
export VISUAL='vim'

# command not found handler
command_not_found_handler() {
    printf '%s: command not found: %s\n' $(basename "$SHELL") "$1"
    read -k "ns?Try with nix-shell? [Y/n] "
    echo

    case "$ns" in
        "n" | "N")
            return 127
            ;;
        *)
            echo "Trying via nix-shell..."
            cmd=$(printf 'nix-shell -p "%s" --run "%s"' "$1" "$*")
            eval "$cmd"
            rc=$?
            return $rc
            ;;
    esac
}

# zsh prompt enable
setopt prompt_subst
PROMPT='%F{green}%n%f@%F{magenta}%m%f %F{blue}%B%~%b%f%F{yellow}$(git_prompt)%f%# '

# Better history search
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY_TIME
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

# open command in $EDITOR
autoload edit-command-line
zle -N edit-command-line
bindkey '^X^e' edit-command-line

# Enable command correction
setopt CORRECT

# Keybindings
# https://zsh.sourceforge.io/Doc/Release/Zsh-Line-Editor.html#Zle-Builtins for functions
# $ showkey -a for key sequences
WORDCHARS='~!#$%^&*(){}[]<>?+;-'
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^H" backward-kill-word
bindkey "^[[3;5~" delete-word
bindkey "\e[3~" delete-char
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
